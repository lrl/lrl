#!/bin/sh
#
#  Copyright © 2011-2014 Samuel Lidén Borell <samuel@kodafritt.se>
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in
#  all copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#  THE SOFTWARE.
#

if [ -z "$1" ]; then
    echo "usage: $0 testsuite" >&2
    exit 2
fi
module="$1"

cd "$(dirname $0)"

cleanup() {
    # Carefully remove tempdir contents, in case variable gets messed up
    rm -f "$tempdir/lrltest"
    rm -f "$tempdir/$module/"*.o
    rmdir "$tempdir/$module"
    [ -e "$tempdir/interop" ] && rmdir "$tempdir/interop"
    rmdir "$tempdir"
}
tempdir=$(mktemp -d)
if [ -z "$tempdir" ]; then
    echo 'mktemp is missing' >&2
    exit 1
fi
trap cleanup 0
mkdir -p "$tempdir/$module"

if [ "$module" != "runtime" ]; then
    valgrind -q --error-exitcode=99 ../lrlc --internal--testmode -I ../../stdlib -c "$module"/*.good --internal--outdir="$tempdir"

    if ls "$module"/*.bad > /dev/null 2>&1; then
        valgrind -q --error-exitcode=99 ../lrlc --internal--testmode --expect-errors -I ../../stdlib "$module"/*.bad
    fi
else
    # Runtime tests
    # This will not work if /tmp is mounted noexec
    for f in $module/*.test.lc; do
        echo "-- $f --" >&2
        valgrind -q ../lrlc -I ../../stdlib "$f" -o "$tempdir/lrltest" &&
        { valgrind -q "$tempdir/lrltest" ||
            echo "Returned error exit code: $?" >&2; }
    done
fi

# supress make error since the tests are currently failing
true # TODO remove

