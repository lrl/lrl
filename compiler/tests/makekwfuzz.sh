#!/bin/sh
#
#  Copyright © 2015 Samuel Lidén Borell <samuel@kodafritt.se>
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in
#  all copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#  THE SOFTWARE.
#


listkeywords() {
    LC_ALL=C sed -r '/^static const KeywordInfo keywords.*$/ b kwstart; d; : kwstart; : kwline; /^\};$/ d; s/^    \{[0-9 ][0-9], "(.*)", *LRL_[A-Za-z0-9_]* \},?$/\1/; n; b kwline' tokenizer.c | { read notused; cat; }
}

liststuff() {
    listkeywords
    cat <<EOF
""
"xyz"
0
1
-1
none
:true
int
[]
[T]
#[
]
(
)
{
}
^
xyz
:abc
xyz:abc
EOF
}

keywords=$(mktemp lrlkwtmp.XXXXXXXXXX) || exit
trap "rm -f -- '$keywords'" EXIT
liststuff >> "$keywords"

while read kw1; do
    printf '%s;\n' "$kw1"
    while read kw2; do
        printf '%s %s;\n' "$kw1" "$kw2"
        while read kw3; do
            printf '%s %s %s;\n' "$kw1" "$kw2" "$kw3"
        done < "$keywords"
    done < "$keywords"
done < "$keywords"


rm -f -- "$keywords"
trap - EXIT
exit

