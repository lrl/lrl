#ifndef SIMPLE_H
#define SIMPLE_H

/*#include <stdlib.h>*/

int var_int;
const int const_int;
int *ptr_int;
int (*ptr_int_2);
long int longint;
unsigned long int ulongint1;
long unsigned int ulongint2;
long unsigned ulongint3;
unsigned long ulongint4;
int (*ptr_to_arr)[2];
extern int unknownsize[];

int simple_add(int a, int b);
int putchar(int c);
int getchar();
int *funcptr_func(int (*fp)(int x));
void test_void(void *a, unsigned char *(*b)[3]);

int *(func());
int (*funcptr)();

void *ptr_void;

void void_function(void);
int void_functionptr(void(*fp)(void));

typedef int typedef_int;
typedef int *typedef_ptr;
typedef int typedef_func();
typedef int (*typedef_funcptr)();

double test_quals(const char *restrict a, char **restrict b);

struct { int *x; int y; } var_struct;
typedef struct { int x; } typedef_struct;
struct { int (*x)[]; int y[2]; } var_struct_arr;
typedef_struct ident_type;

union { int x; float y; } var_union;
typedef union { int x; float y; } typedef_union;
union { int x; char y[1+2]; } var_union_arr;
typedef_union ident_type_union;

void test_attr() __attribute__((__nothrow__, __leaf__)) __attribute__((__pure__)) __attribute__((__nonnull__(1))) ;

struct privstruct;
struct privstruct *a;
struct tagstruct { int x; };
struct tagstruct b;

enum tagenum1 { TAGENUM1_A, TAGENUM1_B };
enum tagenum2 { TAGENUM2_A=123, TAGENUM2_B };
enum tagenum3 { TAGENUM3_A, TAGENUM3_B = 456 };
enum enumexpr { EE1 = (1)+2, EE2 = 3+4, EE3 = (2+3), EE4 = 2+EE3 };
/*enum enumexpr2 { EE21 = (1)+2, EE22 = 3+4, EE23 = ((1)?2+3:4<<5) };*/
enum { BARE_ENUM_A, BARE_ENUM_B, BARE_ENUM_C = BARE_ENUM_B+1 };
int enumarr[EE1];

union privunion;
union privunion *u;
union tagunion { int x; float y; };
union tagunion v;

int testvarargs(int x, ...);
/*int testasm() __asm__("junk" "junk") __attribute__(("blabla"));*/

typedef void wierdtypedef;

/* Test namespace prefix stripping */
int SOME_PREFIX_spstuff;
struct SOME_PREFIX_spstruct1 { int x; };
typedef struct { int x; } SOME_PREFIX_spstruct2;
typedef struct SOME_PREFIX_spstruct3a { int x; } SOME_PREFIX_spstruct3b;
enum SOME_PREFIX_spenum1 { SOME_PREFIX_SPENUM1_VAL = 1 };
typedef enum { SOME_PREFIX_SPENUM2_VAL = 2 } SOME_PREFIX_spenum2;
typedef enum SOME_PREFIX_spenum3a {
    SOME_PREFIX_SPENUM3_VAL = SOME_PREFIX_SPENUM2_VAL
} SOME_PREFIX_spenum3b;

/* Test sizeof and _Alignof */
int (*sizeof1)[sizeof(short)];
int (*sizeof2)[sizeof(char*)];
int (*sizeof3)[sizeof(int *(()))];
typedef struct {
    int a[1 + 2 + sizeof(int) + 10];
} sizeof_struct1;

/* Test of private type */
typedef struct priv_t priv_t;
void usepriv(priv_t *privparam);

int testfb() {
    return 0;
}
int end;

#endif

