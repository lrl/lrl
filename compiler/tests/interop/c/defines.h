#define TEST1
extern int x1;
/* LRL should be able to parse these */
#define TEST2 1
extern int x2;
#define TEST3 0x123
extern int x3;
#define   TEST4    0x456      
extern int x4;
#define TEST5 TEST4
extern int x5;
#define TEST6 \
    1
/* But (currently) not these */
#define TEST7 "hello"
extern int x6;
#define TEST8 (1)
extern int x7;
extern int x8;
#define TEST9 x\
    y\
    \
    "hello" \
    z
extern int x9;
#define TEST10   
extern int x10;
#define TEST11 +
extern int x11;
#define TEST12 \x
extern int x12;
#define TEST13 #define
extern int x13;
#define TEST14(n) 1
extern int x14;


