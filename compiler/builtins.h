/*

  builtins.h -- Builtin types.

  Copyright © 2011-2016 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#ifndef LRL_BUILTINS_H
#define LRL_BUILTINS_H

#include "identifier.h"

typedef enum {
    LRL_BT_count,
    LRL_BT_wcount,
    
    LRL_BT_uint,
    LRL_BT_wuint,
    LRL_BT_int,
    LRL_BT_eint,
    
    LRL_BT_short,
    LRL_BT_ushort,
    LRL_BT_wushort,
    LRL_BT_eshort,
    LRL_BT_long,
    LRL_BT_ulong,
    LRL_BT_wulong,
    LRL_BT_elong,
    LRL_BT_longlong,
    LRL_BT_ulonglong,
    LRL_BT_wulonglong,
    LRL_BT_elonglong,
    
    LRL_BT_char,
    LRL_BT_byte,
    LRL_BT_uint8,
    LRL_BT_wuint8,
    LRL_BT_int8,
    LRL_BT_eint8,
    LRL_BT_uint16,
    LRL_BT_wuint16,
    LRL_BT_int16,
    LRL_BT_eint16,
    LRL_BT_uint32,
    LRL_BT_wuint32,
    LRL_BT_int32,
    LRL_BT_eint32,
    LRL_BT_uint64,
    LRL_BT_wuint64,
    LRL_BT_int64,
    LRL_BT_eint64,
    LRL_BT_uint128,
    LRL_BT_wuint128,
    LRL_BT_int128,
    LRL_BT_eint128,
    
    LRL_BT_float,
    LRL_BT_float16,
    LRL_BT_float32,
    LRL_BT_float64,
    LRL_BT_float80,
    LRL_BT_float128,
    
    LRL_BT_cfloat,
    LRL_BT_cdouble,
    LRL_BT_clongdouble,
    
    LRL_BT_bool,
    
    LRL_BT_NumTypes
} LRLBuiltinType;
#define LRL_BT_INVALID ((LRLBuiltinType)(-1))

#define LRL_case_bt_signedints \
    case LRL_BT_int: \
    case LRL_BT_short: \
    case LRL_BT_long: \
    case LRL_BT_longlong: \
    case LRL_BT_int8: \
    case LRL_BT_int16: \
    case LRL_BT_int32: \
    case LRL_BT_int64: \
    case LRL_BT_int128:
/*#define LRL_case_bt_unsignedints_no_eints \ */
#define LRL_case_bt_uints \
    case LRL_BT_count: \
    case LRL_BT_uint: \
    case LRL_BT_char: \
    case LRL_BT_byte: \
    case LRL_BT_ushort: \
    case LRL_BT_ulong: \
    case LRL_BT_ulonglong: \
    case LRL_BT_uint8: \
    case LRL_BT_uint16: \
    case LRL_BT_uint32: \
    case LRL_BT_uint64: \
    case LRL_BT_uint128:
#define LRL_case_bt_wuints \
    case LRL_BT_wcount: \
    case LRL_BT_wuint: \
    case LRL_BT_wushort: \
    case LRL_BT_wulong: \
    case LRL_BT_wulonglong: \
    case LRL_BT_wuint8: \
    case LRL_BT_wuint16: \
    case LRL_BT_wuint32: \
    case LRL_BT_wuint64: \
    case LRL_BT_wuint128:
#define LRL_case_bt_eints \
    case LRL_BT_eint: \
    case LRL_BT_eshort: \
    case LRL_BT_elong: \
    case LRL_BT_elonglong: \
    case LRL_BT_eint8: \
    case LRL_BT_eint16: \
    case LRL_BT_eint32: \
    case LRL_BT_eint64: \
    case LRL_BT_eint128:
#define LRL_case_bt_unsignedints \
    LRL_case_bt_uints \
    LRL_case_bt_wuints \
    LRL_case_bt_eints
#define LRL_case_bt_floats \
    case LRL_BT_float: \
    case LRL_BT_float16: \
    case LRL_BT_float32: \
    case LRL_BT_float64: \
    case LRL_BT_float80: \
    case LRL_BT_float128: \
    case LRL_BT_cfloat: \
    case LRL_BT_cdouble: \
    case LRL_BT_clongdouble:
#define LRL_case_bt_bool \
    case LRL_BT_bool:
#define LRL_case_bt_specials \
    case LRL_BT_NumTypes:

typedef enum {
    LRL_BTG_signed = 1,
    LRL_BTG_unsigned,
    LRL_BTG_eint, /* either signed or unsigned, e.g. 100 is a eint8, but 200
                     is a uint8 (unsigned) and -100 is a int8 (signed). */
    LRL_BTG_wrapping,
    LRL_BTG_float,
    LRL_BTG_bool
} LRLBuiltinGroup;

typedef struct {
    /* minimum and maximum sizes of the type in bits */
    unsigned int min_int : 8;
    unsigned int max_int : 8;
    unsigned int min_float : 8;
    unsigned int max_float : 8;
    unsigned int is_signed : 1;
    unsigned int special : 4; /* e.g. bool */
    unsigned int series : 4; /* platform-dependent integers, floats, etc */
    unsigned int index : 4;  /* index in series */
    unsigned int btgroup : 4; /* one of the LRL_BTG_* constants */
} LRLBuiltinInfo;

extern const LRLBuiltinInfo lrl_builtin_info[LRL_BT_NumTypes];

extern const LRLToken lrl_builtin_token_zero;
extern const LRLToken lrl_builtin_token_one;

const char *lrl_builtin_get_name(LRLBuiltinType type);
struct LRLASTType *lrl_builtin_get_type(LRLBuiltinType type);
int lrl_is_builtin(const struct LRLASTType *type);
const LRLIdent *lrl_builtin_get_bool_value(int value);
void lrl_builtins_add(LRLIdent *root);
LRLBuiltinType lrl_builtin_determine_int_type(const LRLCodeLocation *loc,
                                              int negative);
int lrl_builtin_target_is_superset(LRLBuiltinType to, LRLBuiltinType from);

#endif

