/*

  backend.h -- The interface that backends/midends have to implement

  Copyright © 2012-2013 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#ifndef LRL_BACKEND_H
#define LRL_BACKEND_H

#include "configfile.h"
#include "parser.h"

typedef enum {
    /** Generate a relocatable file / object file */
    LRL_BO_MakeObject = 1,
    /** Generate an executable file */
    LRL_BO_MakeExec,
    /** Dump backend specific data, such as the generated
        C code in the C backend. */
    LRL_BO_DumpIR
} LRLBackendOperation;

typedef struct {
    LRLConfig *config;
    LRLBackendOperation op;
    const char *input, *output;
} LRLBackendOptions;

typedef struct {
    /* Information */
    const char *name;
    
    /* Functions */
    int (*process)(LRLCtx *ctx, LRLASTNamespace *root,
                   const LRLBackendOptions *options);
} LRLBackend;

/* Set defaults */
#if !defined(ENABLE_BACKEND_CTRANS)
#define ENABLE_BACKEND_CTRANS 1
#endif
#if !defined(ENABLE_BACKEND_MINICG)
#define ENABLE_BACKEND_MINICG 1
#endif

#define LRL_BACKEND_COUNT  (1+(ENABLE_BACKEND_CTRANS+ENABLE_BACKEND_MINICG))
#define LRL_BACKEND_DEFAULT 0
void lrl_backend_get_list(const LRLBackend **list);


#endif

