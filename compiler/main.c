/*

  main.c -- The entry point of the LRL compiler.

  Copyright © 2011-2016 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "backend.h"
#include "configfile.h"
#include "context_private.h"
#include "display.h"
#include "filesys.h"
#include "misc.h"
#include "platform.h"
#include "tokenizer.h"
#include "parser.h"
#include "verify.h"

static void cmdline_error(const char *format, ...)
{
    va_list args;
    
    va_start(args, format);
    fprintf(stderr, "lrlc: error: ");
    vfprintf(stderr, format, args);
    fprintf(stderr, "\n");
    va_end(args);
}

static int try_get_arg(char **dest, const char *description,
                       int *argc, char ***argv)
{
    char **av = *argv;
    int ac = *argc;
    
    if (av[-1][2]) {
        *dest = &av[-1][2];
        return 0;
    } else if (ac != 0) {
        *dest = av[0];
        ++*argv;
        --*argc;
        return 0;
    } else {
        cmdline_error("missing %s argument for -%c",
                      description, av[-1][1]);
        return 2;
    }
}

#define get_arg(destvar, description) do { \
    if (try_get_arg(destvar, description, &argc, &argv)) return 2; \
} while (0)

/**
 * Returns 1 if arg matches either --optname or --optname=xxx.
 * The optname parameter should contain the "--" but not the "=".
 */
static int is_longopt(const char *arg, const char *optname)
{
    size_t optlen = strlen(optname);
    if (strncmp(arg, optname, optlen) != 0) return 0;
    return arg[optlen] == '\0' || arg[optlen] == '=';
}

/**
 * Extracts the value of a long option. argv must point to a long option,
 * either as two strings "--longopt", "value" or as a single string
 * "--longopt=value".
 */
static char *get_longopt_value(int *argc, char ***argv)
{
    char *end = strchr((*argv)[-1], '=');
    if (end) {
        return end+1;
    } else if (*argc) {
        (*argc)--;
        (*argv)++;
        return (*argv)[-1];
    } else {
        return NULL;
    }
}

typedef struct InputEntry InputEntry;
struct InputEntry {
    InputEntry *next;
    
    /* TODO should support intermediate code also */
    char *filename;
};

typedef enum {
    CM_AllToOneOutput = 0, /* default */
    CM_CompileOnly = 1, /* -c */
    CM_ExpectErrors = 2 /* --expect-errors */
} CompileMode;

typedef enum {
    DT_None = 0,
    DT_Tokens,
    DT_AST,
    DT_Idents,
    DT_InteropAST,
    DT_InteropIdents,
    DT_IR
} DumpType;

static const LRLBackend *backends[LRL_BACKEND_COUNT];
static const LRLBackend *backend;
static LRLConfig *config;
static InputEntry *inputs;
static const LRLPath *includedirs;
static CompileMode mode = CM_AllToOneOutput;
static DumpType dump_type;
static char *output = NULL;
static char *outputdir = NULL;
static char *rootdir = ".";
static int quiet, testmode;

static LRLIdent *root_scope;


static NORETURN void show_help(const char *progname)
{
    /* List of backends */
    char blist[34]; /* amount that fits on the screen */
    char *listp = blist;
    size_t i;
    
    for (i = 0; i < LRL_BACKEND_COUNT; i++) {
        if (i > 0) {
            listp += sprintf(listp, i == LRL_BACKEND_COUNT-1 ?
                " or " : ", ");
        }
        listp += sprintf(listp, "%s", backends[i]->name);
    }
    
    printf(
"Usage: %s [options] file...\n"
"\n"
"Options:\n"
"  --backend NAME   Selects the backend to use: %s\n"
"  -c               Compile object files (.o), instead of an executable.\n"
"  --dump INFO      Dumps info and exits. Common values: ast, idents, ir\n"
"  -h, --help       Shows this help text.\n", progname, blist);
    printf(
"  -I PATH          Adds an include search path.\n"
"  -q, --quiet      Don't print any error messages.\n"
"  -r PATH          Sets the root path of the project being compiled.\n"
"  -o FILENAME      Sets the output file.\n"
"\n");
    
    exit(0);
}

static int set_mode(CompileMode newmode)
{
    if (mode) {
        cmdline_error("multiple modes specified (-c and --expect-errors)");
        return 2;
    }
    mode = newmode;
    return 0;
}

static int parse_args(int argc, char **argv)
{
    const char *progname = argv[0];
    InputEntry *last_input = NULL;
    int parse_opts = 1;
    size_t i;
    argc--;
    argv++;
    
    while (argc) {
        
        char *arg = *(argv++);
        argc--;
        
        if (parse_opts && arg[0] == '-' && arg[1]) {
            switch (arg[1]) {
                case '-':
                    if (!arg[2]) {
                        /* -- = stop parsing command line options */
                        parse_opts = 0;
                    } else if (is_longopt(arg, "--dump")) {
                        /* Dump type */
                        if ((arg = get_longopt_value(&argc, &argv)) == NULL)
                            goto invalid_dump_type;
                        
                        if (!strcmp(arg, "tokens")) dump_type = DT_Tokens;
                        else if (!strcmp(arg, "ast")) dump_type = DT_AST;
                        else if (!strcmp(arg, "idents")) dump_type = DT_Idents;
                        else if (!strcmp(arg, "interop_ast")) dump_type = DT_InteropAST;
                        else if (!strcmp(arg, "interop_idents")) dump_type = DT_InteropIdents;
                        else if (!strcmp(arg, "ir")) dump_type = DT_IR;
                        else if (!strcmp(arg, "none")) dump_type = DT_None;
                        else goto invalid_dump_type;
                        break;
                        
                      invalid_dump_type:
                        cmdline_error("invalid dump type. should be "
                                  "\"tokens\", \"ast\", \"idents\", "
                                  "\"interop_ast\", \"interop_idents\", "
                                  "\"ir\" or \"none\".");
                        return 2;
                    } else if (is_longopt(arg, "--backend")) {
                        /* Which backend to use */
                        if ((arg = get_longopt_value(&argc, &argv)) != NULL) {
                            for (i = 0; i < LRL_BACKEND_COUNT; i++) {
                                if (!strcmp(backends[i]->name, arg)) {
                                    backend = backends[i];
                                    goto valid_backend_name;
                                }
                            }
                        }
                        
                        cmdline_error("unknown backend name. "
                                      "Supported backends are:");
                        for (i = 0; i < LRL_BACKEND_COUNT; i++) {
                            fprintf(stderr, "    %s\n", backends[i]->name);
                        }
                        return 2;
                        
                      valid_backend_name:
                        break;
                    } else if (is_longopt(arg, "--internal--outdir")) {
                        /* Output path that is prefixed to the output files
                           (used internally for testing the compiler without
                            spawning a new process for each test file) */
                        if ((arg = get_longopt_value(&argc, &argv)) == NULL) {
                            cmdline_error("--output-dir requires an argument.");
                            return 2;
                        } else {
                            outputdir = arg;
                        }
                    } else if (!strcmp(arg, "--internal--testmode")) {
                        testmode = 1;
                    } else if (!strcmp(arg, "--expect-errors")) {
                        /* Used for running tests of the compiler */
                        if (set_mode(CM_ExpectErrors) != 0) return 2;
                    } else if (!strcmp(arg, "--quiet")) {
                        quiet = 1;
                    } else if (!strcmp(arg, "--help")) {
                        show_help(progname);
                    } else {
                        cmdline_error("invalid option: %s", arg);
                        return 2;
                    }
                    break;
                case 'c':
                    /* compile only */
                    if (arg[2]) goto invalid_option;
                    if (set_mode(CM_CompileOnly) != 0) return 2;
                    break;
                case 'h':
                    /* help */
                    show_help(progname);
                    break;
                case 'I': {
                    /* include path */
                    char *path;
                    LRLPath *entry;
                    get_arg(&path, "include path");
                    
                    entry = malloc(sizeof(LRLPath));
                    entry->next = includedirs;
                    entry->path = path;
                    includedirs = entry;
                    break; }
                case 'r':
                    /* root directory */
                    get_arg(&rootdir, "root directory");
                    break;
                case 'q':
                    /* quiet mode */
                    quiet = 1;
                    break;
                case 'o':
                    /* output */
                    get_arg(&output, "output file");
                    break;
                default:
                invalid_option:
                    cmdline_error("invalid option: %s", arg);
                    return 2;
            }
        } else {
            /* A filename argument (or "-") */
            InputEntry *entry = malloc(sizeof(InputEntry));
            entry->next = NULL;
            entry->filename = arg;
            
            if (last_input) last_input->next = entry;
            else inputs = entry;
            
            last_input = entry;
        }
    }
    
    if (!inputs) {
        cmdline_error("no input files specified.");
        return 2;
    }
    
    return 0;
}


typedef enum {
    LRL_FF_Implementation = 0x1,
    LRL_FF_Optional =       0x2
} LRLFileFlags;

static LRLASTNamespace *parse_file(LRLCtx *ctx, char *filename,
                                   LRLFileFlags flags);


/* TODO can this function be merged with lrl_filesys_ensure_loaded? */
static LRLASTNamespace *load_headers(LRLCtx *ctx, const char *filename)
{
    LRLASTNamespace *header_root;
    char *newname;
    
    /* Determine path without file extension */
    const char *nameend = strchr(filename, '.');
    size_t namelen = (nameend ? (size_t)(nameend - filename) : strlen(filename));
    
    if (nameend && strncmp(nameend, ".lh", 3) == 0) {
        /* This is a header. Don't load it twice! */
        /* XXX should .lh and .lc be changed to .lpub and .lpriv?
           .lh files are quite similar to C headers, but they are not
           duplicated in LRL. For example, they can be used to expose the
           implementation of functions, to allow for optional inlining or
           optimizations. */
        return NULL;
    }
    
    /* Load the .lh file */
    newname = malloc(namelen+4); /* name.lh\0 */
    memcpy(newname, filename, namelen);
    memcpy(newname+namelen, ".lh", 4);
    header_root = parse_file(ctx, newname, LRL_FF_Optional);
    
    /* newname is in use by this file now.
       it should not be modified or freed! */
    
    /* Load all .lh files in the directory */
    /* TODO */
    
    /*free(newname);*/
    return header_root;
}

static LRLASTNamespace *parse_file(LRLCtx *ctx, char *filename,
                                   LRLFileFlags flags)
{
    LRLIdent *scope;
    LRLASTNamespace *root, *header;
    
    /* Determine namespace from path */
    /* TODO resolve ../ to real directories and remove ./  */
    scope = lrl_ident_insert_path(ctx, root_scope, filename);
    scope->flags = (scope->flags & ~LRL_IdFl_NotLoaded) | LRL_IdFl_HasHere;
    
    if (flags & LRL_FF_Implementation) {
        /* Create a private scope for the implementation */
        scope = lrl_ident_create_priv_scope(scope);
        scope->flags |= LRL_IdFl_HasHere | LRL_IdFl_Implementation;
        scope->hash = scope->scope->hash;
        
        /* Load the interface (the header file) */
        header = load_headers(ctx, filename);
    }
    
    /* Load/parse the file into the scope */
    root = lrl_filesys_load_file(ctx, scope, filename,
                         !(flags & LRL_FF_Optional), /* must_exist */
                         dump_type == DT_Tokens); /* dump tokens only */
    
    if (!root) return NULL;
    
    lrl_ctx_bind_verify(ctx);
    
    if (flags & LRL_FF_Implementation) {
        /* Check compatibility of definitions */
        /* TODO */
    }
    
    if (dump_type == DT_AST) {
        lrl_display_deflist(root->list, 0);
        return NULL;
    } else if (dump_type == DT_InteropAST) {
        lrl_display_deflist_interops(root->list);
        return NULL;
    } else if (dump_type == DT_Idents) {
        lrl_display_scope(scope, 0);
        return NULL;
    } else if (dump_type == DT_InteropIdents) {
        /* interop identifiers are dumped after all files have been processed */
        return NULL;
    }
    
    return root;
}

/**
 * The "root scope" is simply an empty scope with the path set to the root
 * path, but it has to include any include directories and the builtin
 * definitions. This function creates the root scope.
 */
static void setup_root_scope(LRLCtx *ctx)
{
    LRLIdent *scope;
    const LRLPath *dir;
    
    /* Root of the project/whatever being compiled */
    root_scope = calloc(1, sizeof(LRLIdent));
    root_scope->path = rootdir;
    root_scope->flags |= LRL_IdFl_NotLoaded | LRL_IdFl_HasHere;
    
    /* Wrap the project root in include dir scopes */
    scope = root_scope;
    for (dir = includedirs; dir; dir = dir->next) {
        LRLIdent *include = calloc(1, sizeof(LRLIdent));
        scope->scope = include;
        include->path = (char*)dir->path;
        include->flags |= LRL_IdFl_NotLoaded;
        scope = include;
    }
    
    /* The outermost scope (the "real" root) is the builtins */
    scope->scope = ctx->builtins_scope;
}

/* Functions for testing of the compiler */
typedef struct {
    int line, optional, count;
} ExpectErrorLine;
static ExpectErrorLine *expect_lines;
static int num_expect_lines;
static const char *expect_filename;
static int unexpected_error;

static void add_expected_line(int line, int optional)
{
    expect_lines = try_realloc(expect_lines, ++num_expect_lines*sizeof(ExpectErrorLine));
    expect_lines[num_expect_lines-1].line = line;
    expect_lines[num_expect_lines-1].optional = optional;
    expect_lines[num_expect_lines-1].count = 0;
}

static void expect_handler(LRLCtx *ctx, const LRLError *err)
{
    const char *filename;
    int line, col;
    
    lrl_ctx_current_error_source(ctx, &filename, &line, &col);

    if (filename && !strcmp(filename, expect_filename)) {
        int i;
        for (i = 0; i < num_expect_lines; i++) {
            if (expect_lines[i].line == line) {
                expect_lines[i].count++;
                return;
            }
        }
        
        /* Display unexpected errors to stderr */
        lrl_err_default_handler(ctx, err);
        unexpected_error = 1;
    }
}

static void silent_handler(LRLCtx *ctx, const LRLError *err)
{
    /* Error message is supressed */
    (void)ctx;
    (void)err;
}


int main(int argc, char **argv)
{
    int error;
    LRLCtx *ctx;
    
    lrl_backend_get_list(backends);
    backend = backends[LRL_BACKEND_DEFAULT];
    
    lrl_platform_init_paths();
    includedirs = lrl_platform_includes;
    
    error = parse_args(argc, argv);
    if (error) return error;
    
    config = lrl_config_new();
    lrl_config_load_system(config);
    
    ctx = lrl_ctx_new(config);
    setup_root_scope(ctx);
    
    if (quiet) {
        lrl_err_set_handler(ctx, silent_handler);
    }
    
    switch (mode) {
    case CM_CompileOnly:
        /* Compile each file separately */
        if (output && (!inputs || inputs->next)) {
            cmdline_error("the -o option in -c mode requires exactly one "
                          "input file.");
            return 2;
        }
        
        for (; inputs; inputs = inputs->next) {
            LRLASTNamespace *file_ast;
            
            lrl_ctx_set_has_errors(ctx, 0);
            if (testmode) {
                fprintf(stderr, "-- %s --\n", inputs->filename);
            }
            file_ast = parse_file(ctx, inputs->filename, LRL_FF_Implementation);
            
            if (lrl_ctx_has_errors(ctx)) {
                error = 1;
                continue;
            }
            
            if (file_ast && (!dump_type || dump_type == DT_IR)) {
                char *defout = (output ?
                    NULL : get_output_filename(inputs->filename,
                                               outputdir, 0));
                LRLBackendOptions options;
                options.config = config;
                options.op = (dump_type == DT_IR ?
                              LRL_BO_DumpIR : LRL_BO_MakeObject);
                options.input = inputs->filename;
                options.output = (output ? output : defout);
                if (backend->process(ctx, file_ast, &options) != 0) {
                    error = 1;
                }
                free(defout);
            } else if (dump_type == DT_InteropIdents) {
                lrl_display_scope(ctx->external_scope, 0);
            }
        }
        lrl_ctx_free(ctx);
        break;
    case CM_AllToOneOutput: {
        /* Compile multiple files into one output */
        LRLBackendOptions options;
        
        LRLASTNamespace ast_root;
        ast_root.ident = NULL;
        ast_root.list = NULL;
        
        for (; inputs; inputs = inputs->next) {
            LRLASTNamespace *namespac = parse_file(ctx, inputs->filename, LRL_FF_Implementation);
            LRLASTDefList *entry;
            
            if (lrl_ctx_has_errors(ctx)) {
                error = 1;
                continue;
            }
            
            if (!namespac) continue;
            
            entry = malloc(sizeof(LRLASTDefList));
            entry->def.ast_type = LRL_AST_Namespace;
            entry->def.kind.namespac = namespac;
            
            entry->next = ast_root.list;
            ast_root.list = entry;
        }
        
        
        if (!error && (!dump_type || dump_type == DT_IR)) {
            options.config = config;
            options.op = (dump_type == DT_IR ?
                          LRL_BO_DumpIR : LRL_BO_MakeExec);
            options.input = NULL;
            options.output = (output ? output : "a.out");
            if (backend->process(ctx, &ast_root, &options) != 0) {
                error = 1;
            }
        } else if (dump_type == DT_InteropIdents) {
            lrl_display_scope(ctx->external_scope, 0);
        }
        
        lrl_ctx_free(ctx);
        break; }
    case CM_ExpectErrors:
        /* Used for running tests of the compiler */
        if (!inputs || output) {
            cmdline_error("the --expect-errors options requires at least "
                          "one input file and no outputs.");
            return 2;
        }
        
        lrl_err_set_handler(ctx, expect_handler);
        for (; inputs; inputs = inputs->next) {
            char *source, *s;
            int line = 1, i;
            int errorblock = 0;
            int eof_error = 0;
            int optional;
            
            if (testmode) {
                fprintf(stderr, "-- %s --\n", inputs->filename);
            }
            lrl_ctx_set_has_errors(ctx, 0);
            unexpected_error = 0;
            
            /* Scan source for lines marked with // ERROR */
            expect_filename = inputs->filename;
            source = read_input(inputs->filename);
            if (!source) {
                perror(inputs->filename);
                continue;
            }
            for (s = source; *s; s += strcspn(s, "/\n\r")) {
                char ch;
                if (!strncmp(s, "// ERROR", 8)) {
                    s += 8;
                    optional = !strncmp(s, " OPTIONAL", 9);
                    add_expected_line(line, optional);
                    continue;
                } else if (!strncmp(s, "// EOF ERROR", 12)) {
                    s += 12;
                    optional = !strncmp(s, " OPTIONAL", 9);
                    eof_error = 1;
                    continue;
                } else if (!strncmp(s, "/* ERRORS", 9)) {
                    s += 9;
                    optional = !strncmp(s, " OPTIONAL", 9);
                    errorblock = line;
                    continue;
                }
                ch = *(s++);
                
                if (errorblock && line > errorblock && (ch == '\n' || ch == '\r')) {
                    add_expected_line(line, optional);
                }
                
                if (ch == '\n') {
                    line++;
                    goto was_linebreak;
                } else if (ch == '\r') {
                    line++;
                    if (*s == '\n') { s++; }
                    goto was_linebreak;
                }
                
                if (0) {
                  was_linebreak:
                    if (errorblock && (strcspn(s, "\r\n") <= strspn(s, " \t};"))) {
                        errorblock = 0;
                    }
                }
            }
            free(source);
            
            if (eof_error || errorblock) {
                add_expected_line(line, optional);
            }
            
            /* Parse file */
            parse_file(ctx, inputs->filename, LRL_FF_Implementation);
            
            /* Check that all errors where reported correctly */
            for (i = 0; i < num_expect_lines; i++) {
                if (expect_lines[i].count == 0 && !expect_lines[i].optional) {
                    if (!quiet) {
                        fprintf(stderr, "%s: no error reported at line %d\n",
                                inputs->filename, expect_lines[i].line);
                    }
                    error = 1;
                }
            }
            error |= unexpected_error;
            
            free(expect_lines);
            expect_lines = NULL;
            num_expect_lines = 0;
            
        }
        lrl_ctx_free(ctx);
        break;
    }
    
    lrl_config_free(config);
    return error; /* 1 or 0 */
}


