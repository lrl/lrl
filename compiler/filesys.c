/*

  filesys.c -- Loads modules from the file system.

  Copyright © 2012-2014 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include "display.h"
#include "filesys.h"
#include "misc.h"

#include <dirent.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>


LRLASTNamespace *lrl_filesys_load_file(LRLCtx *ctx, LRLIdent *scope,
                                       char *filename, int must_exist,
                                       int dump_tokens)
{
    char *contents;
    LRLToken *tokens;
    size_t num_tokens;
    LRLASTNamespace *root;
    LRLFileInfo *fileinfo;
    
    /* Read file */
    contents = read_input(filename);
    if (!contents) {
        if (must_exist || (errno != ENOENT && errno != ENOTDIR)) {
            perror(filename);
            lrl_ctx_set_has_errors(ctx, 1);
        }
        return NULL;
    }
    
    /* Add to context */
    fileinfo = lrl_ctx_add_file(ctx, filename, contents);
    
    /* Tokenize */
    num_tokens = lrl_tokenize(ctx, &tokens, contents);
    if (dump_tokens) {
        lrl_display_tokens(tokens, num_tokens);
        return NULL;
    }
    
    /* Parse */
    root = lrl_parse(ctx, scope, tokens);
    
    lrl_ctx_set_file_ast(fileinfo, root);
    return root;
}

static size_t ident_length(const LRLIdent *ident)
{
    if (ident->def_token) return ident->def_token->loc.length;
    else if (ident->name) return strlen(ident->name);
    else return (size_t)-1;
}

static const char *ident_name(const LRLIdent *ident)
{
    if (ident->def_token) return ident->def_token->loc.start;
    else return ident->name;
}

static int is_header(const char *name)
{
    size_t len = strlen(name);
    return len >= 4 ? !strcmp(&name[len-3], ".lh") : 0;
}

int lrl_filesys_ensure_loaded(LRLCtx *ctx, LRLIdent *namespac)
{
    LRLIdent *scope;
    size_t mountlen, identlen = 0;
    char *path, *end;
    int found = 0;
    DIR *dir;
    
    if ((namespac->flags & LRL_IdFl_NotLoaded) == 0) {
        fail("flsensureloaded_loadedtwice");
    }
    namespac->flags &= ~LRL_IdFl_NotLoaded;
    
    /* Determine "mount point" */
    scope = namespac;
    while (!scope->path) {
        identlen += ident_length(scope) + 1;  /* "name/" */
        scope = (LRLIdent*)scope->scope; /* containing scope */
        if (!scope) fail("flsensureloaded_roothaspath");
    }
    
    /* Build base path */
    mountlen = strlen(scope->path);
    path = malloc(mountlen + identlen + 3 + 1); /* extra space for ".lh" */
    memcpy(path, scope->path, mountlen);
    
    end = &path[mountlen + identlen];
    end[0] = '\0';
    
    scope = namespac;
    while (!scope->path) {
        size_t namelen = ident_length(scope);
        
        if (namelen != (size_t)-1) {
            end -= namelen;
            memcpy(end, ident_name(scope), namelen);
            end--;
            end[0] = '/';
        }
        
        scope = (LRLIdent*)scope->scope;
    }
    
    /* Check if it's a directory */
    dir = opendir(path);
    if (!dir) {
        errno = 0;
    } else {
        struct dirent *ent;
        while ((ent = readdir(dir)) != NULL) {
            LRLIdent *ident;
            
            /* Skip hidden files */
            if (ent->d_name[0] == '.') continue;
            
            /* Skip files that don't end with .lh and that aren't directories */
            if (!is_header(ent->d_name) && strchr(ent->d_name, '.')) continue;
            
            ident = lrl_ident_insert_path(ctx, namespac, ent->d_name);
            ident->flags |= LRL_IdFl_HasHere;
        }
        closedir(dir);
        found = 1;
    }
    
    /* Check if it's a source file */
    if (identlen) {
        strcpy(&path[mountlen+identlen], ".lh");
        if (lrl_filesys_load_file(ctx, namespac, path, 0, 0)) {
            found = 1;
        } else {
            free(path);
        }
    } else {
        free(path);
    }
    
    return found;
}


