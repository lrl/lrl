/*

  display.h -- Functions to print various structures.

  Copyright © 2011-2015 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/


#ifndef LRL_DISPLAY_H
#define LRL_DISPLAY_H

#include "parser.h"

const char *lrl_display_get_ast_name(LRLASTNodeType ast_type);

void lrl_display_loc(const LRLCodeLocation *loc);
void lrl_display_token_type(LRLTokenType type);
void lrl_display_token(const LRLToken *token);
void lrl_display_tokens(const LRLToken *tokens, size_t num_tokens);
void lrl_display_ident(const LRLIdent *ident);
void lrl_display_identref(const LRLIdentRef *identref);
void lrl_display_expr(const LRLASTExpr *expr);

void lrl_display_qualifiers(LRLTypeQualifiers quals);
void lrl_display_type(const LRLASTType *type, int indent);
void lrl_display_typelist(const LRLASTTypeList *list, int indent);
void lrl_display_typenames(const LRLASTDefList *list, int indent);
void lrl_display_defflags(const LRLDefFlags flags);

void lrl_display_def_type(const LRLASTDefType *def, int indent);
void lrl_display_def_data(const LRLASTDefData *def, int indent);
void lrl_display_def_function(const LRLASTDefFunction *def, int indent);
void lrl_display_uses(const LRLASTUses *uses, int indent);
void lrl_display_namespace(const LRLASTNamespace *namespac, int indent);
void lrl_display_interop(const LRLASTInterop *interop, int indent);
void lrl_display_deflist(const LRLASTDefList *list, int indent);
void lrl_display_deflist_interops(const LRLASTDefList *list);

void lrl_display_statement(const LRLASTStmt *stmt, int indent);

void lrl_display_scope(const LRLIdent *scope, int indent);

#endif

