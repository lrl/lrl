/*

  verify.h -- Checks that the source is semantically valid.

  Copyright © 2012-2016 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#ifndef LRL_VERIFY_H
#define LRL_VERIFY_H

#include "context.h"
#include "parser.h"

/** Flags when comparing type compatibility */
typedef enum {
    /** Types must be equal, not e.g. int and byte */
    LRL_TCF_RequireEqual = 0x1,
    /** This is a typeassert conversion. This allows a smaller
        destination type */
    LRL_TCF_IsTypeAssert = 0x2,
    /** Either type must be assignable to the other */
    LRL_TCF_Symmetric    = 0x4
} LRLTypeCompatFlags;

/* Functions for dealing with typerefs */
LRLTypeRef typeref_root(const LRLASTType *type,
                        LRLTypeQualifiers default_quals);
LRLTypeRef typeref_nested(const LRLASTType *type,
                          const LRLTypeRef *subtyperef);

/**
 * Verifies definitions, expressions, types, etc. Call this function until
 * lrl_ident_has_deferred() returns false.
 */
void lrl_vfy_namespace(LRLCtx *ctx, LRLASTNamespace *root);

int lrl_vfy_type_compat(LRLCtx *ctx,
                        const LRLTypeRef *to_ref, const LRLTypeRef *from_ref,
                        const LRLASTExpr *error_expr,
                        LRLTypeCompatFlags flags);

LRLTypeRef lrl_vfy_find_real_typeref(const LRLTypeRef *typeref);
const LRLASTType *lrl_vfy_find_real_type(const LRLASTType *type);

int lrl_vfy_is_private_type(LRLCtx *ctx, const LRLTypeRef *typeref);

int lrl_vfy_substitute_type_params(const LRLASTType **type,
                                   LRLBoundParams *params);
int lrl_vfy_bind_type_params(const LRLASTType **typeptr,
                             LRLBoundParams **params);


#endif

