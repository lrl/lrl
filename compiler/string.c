/*

  string.c -- String parser

  Copyright © 2012-2016 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include "string.h"

#include <string.h>

static void err_char(LRLCtx *ctx, LRLErrorType code, const char *source)
{
    if (ctx) {
        lrl_err_char(ctx, code, source);
    }
}

static int unhex(LRLCtx *ctx, const char **sourcePtr)
{
    int ch = *(*sourcePtr)++;
    if (ch >= '0' && ch <= '9') return ch - '0';
    else if (ch >= 'a' && ch <= 'f') return 0xa + (ch - 'a');
    else if (ch >= 'A' && ch <= 'F') return 0xa + (ch - 'A');
    else {
        err_char(ctx, LRL_Err_InvalidCharacterEscape, *sourcePtr);
        return -1;
    }
}

static void unescape_char(LRLCtx *ctx, const char **sourcePtr, char **resultPtr)
{
    char rch;
    int numdigits;
    
    switch (*(*sourcePtr)++) {
        case '0': rch = '\0'; break; /* octal codes are not allowed */
        case '\\': rch = '\\'; break;
        case '\"': rch = '\"'; break;
        case 'a': rch = '\a'; break;
        case 'b': rch = '\b'; break;
        case 'f': rch = '\f'; break;
        case 'n': rch = '\n'; break;
        case 'r': rch = '\r'; break;
        case 't': rch = '\t'; break;
        case 'v': rch = '\v'; break;
        case 'x': {
            /* Hexadecimal */
            int high, low;
            if ((high = unhex(ctx, sourcePtr)) < 0 ||
                (low = unhex(ctx, sourcePtr)) < 0) return;
            
            rch = (high<<4) | low;
            break; }
        case 'u':
            numdigits = 4;
            goto unicode;
        case 'U':
            numdigits = 8;
          unicode: {
            /* Unicode UCS-2 ("u") or UCS-4 ("U") in hex */
            unsigned long code = 0;
            int i;
            
            for (i = 0; i < numdigits; i++) {
                int hex = unhex(ctx, sourcePtr);
                if (hex < 0) return;
                code = (code<<4) | hex;
            }
            
            if (code > 0x10FFFF) {
                err_char(ctx, LRL_Err_InvalidUnicodeCodepoint, *sourcePtr);
                return;
            }
            
            /* Add to result string in UTF-8 format */
            if (resultPtr) {
                unsigned char *res = (unsigned char*)*resultPtr;
                if (code <= 0x7F) {
                    *(res++) = code;
                } else if (code <= 0x7FF) {
                    *(res++) = 0xC0 | (code >> 6);
                    *(res++) = 0x80 | (code & 0x3F);
                } else if (code <= 0xFFFF) {
                    *(res++) = 0xE0 | (code >> 12);
                    *(res++) = 0x80 | ((code >> 6) & 0x3F);
                    *(res++) = 0x80 | (code & 0x3F);
                } else {
                    *(res++) = 0xF0 | (code >> 18);
                    *(res++) = 0x80 | ((code >> 12) & 0x3F);
                    *(res++) = 0x80 | ((code >> 6) & 0x3F);
                    *(res++) = 0x80 | (code & 0x3F);
                }
                *resultPtr = (char*)res;
            }
            return; }
        default:
            err_char(ctx, LRL_Err_InvalidCharacterEscape, *sourcePtr-1);
            return;
    }
    
    if (resultPtr) {
        *((*resultPtr)++) = rch;
    }
}

/**
 * Unescapes escape codes in a string. When calling this function, sourcePtr
 * must be the first character inside the string (not the "), and resultPtr
 * -- if specified -- must be an already allocated string at least as large
 * as the input string, which will receive the unescaped result.
 *
 * After returning, sourcePtr will point to end+1 (i.e. the ") of the string
 * and resultPtr will point to end+1 of the resulting unescaped string.
 * The return value is 1 on success and 0 on failure.
 */
int lrl_string_parse(LRLCtx *ctx, const char **sourcePtr, char **resultPtr)
{
    const char *source = *sourcePtr;
    int result;
    
    for (;;) {
        char ch;
        size_t skiplen = strcspn(source, "\"\\\n/\r*");
        if (resultPtr) {
            memcpy(*resultPtr, source, skiplen);
            *resultPtr += skiplen;
        }
        source += skiplen;
        ch = *(source++);
        
        switch (ch) {
            case '\n':
            case '\r':
            case '\0':
                source--;
                err_char(ctx, LRL_Err_UnclosedString, *sourcePtr-1);
                result = 0;
                goto end;
            case '"':
                /* End of string */
                result = 1;
                goto end;
            case '\\':
                unescape_char(ctx, &source, resultPtr);
                break;
            case '/':
                /* Start of comment sequence must be escaped as "/\*" */
                if (resultPtr) { *((*resultPtr)++) = '/'; }
                if (*source == '*') {
                    err_char(ctx, LRL_Err_CommentStartMustBeEscaped,
                             *sourcePtr);
                } else if (*source == '\\' && source[1] == '*') {
                    source += 2;
                    if (resultPtr) { *((*resultPtr)++) = '*'; }
                }
                break;
            case '*':
                /* End of comment sequence must be escaped as "*\/" */
                if (resultPtr) { *((*resultPtr)++) = '*'; }
                if (*source == '/') {
                    err_char(ctx, LRL_Err_CommentEndMustBeEscaped,
                             *sourcePtr);
                } else if (*source == '\\' && source[1] == '/') {
                    source += 2;
                    if (resultPtr) { *((*resultPtr)++) = '/'; }
                }
        }
    }
    
  end:
    *sourcePtr = source;
    return result;
}

int lrl_strings_equal(const LRLCodeLocation *a, const LRLCodeLocation *b)
{
    char *unescaped1, *uend1, *unescaped2 = NULL, *uend2;
    const char *eend;
    size_t ulen1, ulen2;
    int result;
    
    /* Fast path for byte-for-byte identical strings */
    if (a->length == b->length && !strncmp(a->start, b->start, a->length))
        return 1;
    
    /* Otherwise, compare unescaped strings */
    result = 0;
    unescaped1 = uend1 = malloc(a->length);
    eend = a->start+1;
    if (!lrl_string_parse(NULL, &eend, &uend1)) goto end;
    
    unescaped2 = uend2 = malloc(b->length);
    eend = b->start+1;
    if (!lrl_string_parse(NULL, &eend, &uend2)) goto end;
    
    ulen1 = uend1 - unescaped1;
    ulen2 = uend2 - unescaped2;
    if (ulen1 != ulen2) goto end;
    
    result = !strncmp(unescaped1, unescaped2, ulen1);
    
  end:
    free(unescaped1);
    free(unescaped2);
    return result;
}



