/*

  misc.h -- Miscellaneous internal utility functons.

  Copyright © 2011-2017 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#ifndef LRL_MISC_H
#define LRL_MISC_H

#include <limits.h>
#include <stdlib.h>
#include <stdio.h>

#include "context.h"

/* Type for pointer comparison */
#if __STDC_VERSION__ < 199901L
#ifdef _WIN64
typedef unsigned long long uintptr_t;
#elif defined(__arm__)
typedef unsigned int uintptr_t;
#else
typedef unsigned long uintptr_t;
#endif
#else
#include <stdint.h>
#endif

/* Array lists */
#define init_list(p, size_var, capacity_var, initial_size) do {\
        *(size_var) = 0; \
        *(capacity_var) = (initial_size); \
        *(p) = malloc((initial_size)*sizeof(**(p))); \
    } while (0)

#define grow_list(p, size_var, capacity_var) \
    (++(*(size_var)) < *(capacity_var) || \
        (*(capacity_var) *= 2 , \
        (*(p) = try_realloc(*(p), (*(capacity_var))*sizeof(**(p)))) != NULL))

#define list_push(p, size_var, capacity_var, value) do { \
        if (!grow_list((p), (size_var), (capacity_var))) fail("list_push"); \
        (*(p))[*(size_var)-1] = (value); \
    } while (0)

#define list_get_last(p, size, value_var) \
    ((size) != 0 && ((*(value_var) = ((p)[(size)-1])), 1))

/* Linked lists */
#define linked_append(entry, first, last) do { \
        *(entry) = malloc(sizeof(**(entry))); \
        (*(entry))->next = NULL; \
        if (*(last) == NULL) { \
            *(first) = *(entry); \
        } else { \
            (*(last))->next = *(entry); \
        } \
        *(last) = *(entry); \
    } while (0)

/**
 * A linearly probed set. It provides fast a insert operation for
 * LRLHashCodes with a check for duplicates.
 */
typedef struct {
    size_t size; /* number of entries = 2^(size-1) */
    LRLHashCode *set;
} LinProbSet;

/* http://www.decompile.com/cpp/faq/file_and_line_error_string.htm */
#define STRINGIFY1(x) #x
#define STRINGIFY(x) STRINGIFY1(x)

/* Safe type conversion */
#define size2int(s) ((s) > INT_MAX ? \
    fail("size2int@" __FILE__ ":" STRINGIFY(__LINE__)),0 : (int)(s))

/* noreturn qualifier for functions */
#if defined(__GNUC__) || defined(__clang__)
#    define NORETURN __attribute__((__noreturn__))
#else
#    define NORETURN
#endif

void *try_realloc(void *p, size_t size);
char *lrl_strdup(const char *s);
char *lrl_strndup(const char *s, size_t len);
LRLHashCode hash_bytes(const unsigned char *bytes, size_t length);
LRLHashCode hash_name(const char *name, size_t length);
LRLHashCode hash_merge(LRLHashCode a, LRLHashCode b);
void linprobset_init(LinProbSet *lps, size_t bits);
int linprobset_add_hashcode(LinProbSet *lps, LRLHashCode hashcode);

void skip_path_sep(const char **path);
const char *end_of_path_elem(const char *path);
char *get_output_filename(const char *source_filename, const char *outdir,
                          int is_exec);

char *file_get_contents(FILE *file);
char *read_input(const char *filename);

NORETURN void fail(const char *error_id);


#endif



