/*

  platform.h -- Platform-specific defines and functions

  Copyright © 2012-2013 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#ifndef LRL_PLATFORM_H
#define LRL_PLATFORM_H

#include <stdlib.h>
#include <stdio.h>

typedef struct LRLPath {
    const struct LRLPath *next;
    const char *path;
} LRLPath;

/* * * * Default values * * * */

/* Quoted strings in shell commands */
#define QUOTECHAR '\''
#define QUOTESTR "\'"
#define PATHSEP "/"

void lrl_platform_init_paths(void);
extern const LRLPath *const lrl_platform_includes;
extern const LRLPath *const lrl_platform_config;

char *join_paths(const char *a, const char *b);
int check_not_dir(FILE *file);
int mark_executable(FILE *file, const char *filename);
int pclose_exit_ok(int status);

/* * * * Windows-specific overrides * * * */
#ifdef _WIN32 /* also 64-bit */

#undef  QUOTECHAR
#define QUOTECHAR '\"'

#undef  QUOTESTR
#define QUOTESTR "\""

#undef PATHSEP
#define PATHSEP "\\"

#endif


#endif


