//
// Copyright © 2014 Samuel Lidén Borell <samuel@kodafritt.se>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

// This file lists identifiers that are defined by the C standards library (the
// parts included by LRL) and should not be generated again even if they are
// declared e.g. in an header included (possibly indirectly) in the interop.
//
// This list includes internal identifiers that are defined by glibc, and
// should be extended with internal identifiers of other common C libraries.
char
short
int
long
_Bool
pthread_*
__pthread_*

__acos
__asin
__atan
__atan2
__bzero
__ceil
__compar_fn_t
__cos
__cosh
__ctype_get_mb_cur_max
__exp
__fabs
__finite
__floor
__fmod
__frexp
__isinf
__isnan
__ldexp
__log
__log10
__modf
__pow
__sin
__sinh
__sqrt
__strtok_r
__tan
__tanh
abort
abs
acos
asin
atan
atan2
atexit
atof
atoi
atol
bsearch
calloc
ceil
cos
cosh
div
div_t
exit
exp
fabs
floor
fmod
free
frexp
getenv
int16_t
int32_t
int64_t
int8_t
int_fast16_t
int_fast32_t
int_fast64_t
int_fast8_t
int_least16_t
int_least32_t
int_least64_t
int_least8_t
intmax_t
intptr_t
labs
ldexp
ldiv
ldiv_t
log
log10
malloc
mblen
mbstowcs
mbtowc
memchr
memcmp
memcpy
memmove
memset
modf
pow
ptrdiff_t
qsort
rand
realloc
sin
sinh
size_t
sqrt
srand
strcat
strchr
strcmp
strcoll
strcpy
strcspn
strerror
strlen
strncat
strncmp
strncpy
strpbrk
strrchr
strspn
strstr
strtod
strtok
strtol
strtoul
strxfrm
system
tan
tanh
uint16_t
uint32_t
uint64_t
uint8_t
uint_fast16_t
uint_fast32_t
uint_fast64_t
uint_fast8_t
uint_least16_t
uint_least32_t
uint_least64_t
uint_least8_t
uintmax_t
uintptr_t
wchar_t
wcstombs
wctomb

