/*

  platform.c -- Platform-specific functions

  Copyright © 2012-2013 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#ifndef _WIN32
#define _POSIX_C_SOURCE 200809L
#define _DEFAULT_SOURCE 1
#define _BSD_SOURCE 1
#endif

#include "platform.h"
#include <string.h>


char *join_paths(const char *a, const char *b)
{
    char *path = malloc(strlen(a)+1+strlen(b)+1);
    sprintf(path, "%s" PATHSEP "%s", a, b);
    return path;
}


/* * * * Windows/Wine/ReactOS * * * */
#ifdef _WIN32 /* also 64-bit */

#include <shlobj.h>
#include <string.h>
#include "misc.h"

const LRLPath *const lrl_platform_includes;

static char *get_sys_path(int pathid, const char *subdir)
{
    /* TODO use wide-char functions and convert to UTF-8 (char*). Then convert
       back to wide-char when loading files, starting processes, etc. */
    char path[MAX_PATH];
    
    HRESULT res = SHGetFolderPath(NULL, pathid, NULL, 0, path);

    if (!SUCCEEDED(res) || strlen(path) >= MAX_PATH-strlen(subdir)-1) return NULL;
    
    strcat(path, subdir);
    return lrl_strdup(path);
}

static LRLPath sys_include = { NULL, NULL };
const LRLPath *const lrl_platform_includes = &sys_include;

static LRLPath dist_config = { NULL, NULL };
static LRLPath sys_config = { &dist_config, NULL };
static LRLPath user_config = { &sys_config, NULL };
const LRLPath *const lrl_platform_config = &user_config;

void lrl_platform_init_paths(void)
{
    sys_include.path = get_sys_path(CSIDL_PROGRAM_FILES_COMMON, "\\LRL Includes");
    
    dist_config.path = get_sys_path(CSIDL_PROGRAM_FILES_COMMON, "\\lrlc\\conf");
    sys_config.path = get_sys_path(CSIDL_COMMON_APPDATA, "\\lrlc");
    user_config.path = get_sys_path(CSIDL_APPDATA, "\\lrlc");
}


int check_not_dir(FILE *file)
{
    (void)file;
    return 1; /* Directories can't be "opened" under Windows */
}

int mark_executable(FILE *file, const char *filename)
{
    (void)file;
    (void)filename;
    return 1;
}

int pclose_exit_ok(int status)
{
    /* pclose returns the exit status directly under Windows. But it
       seems that the exit status is not propagated reliably. E.g.
       when I tested this function with GCC under Wine the status code
       was always zero.  -- SLB 2013-08-31 */
    return status == 0;
}

#else
/* * * * Others. Assume POSIX compliant * * * */

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>


/* TODO should include the include dir specified in the Makefile, if its
        neither /usr/share/lrl-includes nor /usr/share/local/lrl-includes */
static const LRLPath usr_include = { NULL, "/usr/share/lrl-includes" };
static const LRLPath local_include = { &usr_include, "/usr/local/share/lrl-includes" };
const LRLPath *const lrl_platform_includes = &local_include;

static const LRLPath dist_config = { NULL, "/usr/share/lrlc/conf" }; /* TODO should use install prefix */
static const LRLPath sys_config = { &dist_config, "/etc/lrlc" };
static LRLPath user_config = { &sys_config, NULL };
const LRLPath *const lrl_platform_config = &user_config;

void lrl_platform_init_paths(void)
{
    const char *xdgconf = getenv("XDG_CONFIG_HOME");
    
    if (xdgconf) {
        user_config.path = join_paths(xdgconf, "lrlc");
    } else {
        const char *home = getenv("HOME");
        if (home) {
            user_config.path = join_paths(home, ".config/lrlc");
        }
    }
    
    /* TODO should use XDG_CONFIG_DIRS also (before XDG_CONFIG_HOME) */
}

int check_not_dir(FILE *file)
{
    struct stat st;
    int fd = fileno(file);
    
    if (fd == -1 || fstat(fd, &st) == -1) return 1;
    
    if (S_ISDIR(st.st_mode)) {
        errno = EISDIR;
        return 0;
    }
    
    return 1;
}

int mark_executable(FILE *file, const char *filename)
{
    struct stat st;
    mode_t mode;
    int fd = fileno(file);
    
    if (fd == -1 || fstat(fd, &st) == -1) goto error;
    
    mode = st.st_mode;
    if (mode & S_IRUSR) mode |= S_IXUSR;
    if (mode & S_IRGRP) mode |= S_IXGRP;
    if (mode & S_IROTH) mode |= S_IXOTH;
    
    if (fchmod(fd, mode) == -1) goto error;
    
    return 1;
    
  error:
    perror(filename);
    return 0;
}

int pclose_exit_ok(int status)
{
    return WIFEXITED(status) && WEXITSTATUS(status) == 0;
}

#endif



