/*

  context.h -- Stores state of the compilation process.

  Copyright © 2011-2016 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#ifndef LRL_CONTEXT_H
#define LRL_CONTEXT_H

#include <limits.h>
#include <stddef.h>
#include <stdlib.h>

#include "configfile.h"


/* Defined elsewhere, but used here */
typedef struct LRLCtx LRLCtx;
typedef struct LRLToken LRLToken;
typedef struct LRLFileInfo LRLFileInfo;
typedef struct LRLASTNamespace LRLASTNamespace;
typedef struct LRLASTDefList LRLASTDefList;
typedef struct LRLASTInterop LRLASTInterop;
typedef struct LRLASTType LRLASTType;
typedef struct LRLTypeRef LRLTypeRef;
typedef struct LRLASTExpr LRLASTExpr;
typedef struct LRLInteropImpl LRLInteropImpl;

/* Hash code */
typedef unsigned long LRLHashCode;

#if ULONG_MAX/32768U/32768U/32768U/32768U >= 15 /* 64 bit long */
#define LRL_HASH_BITS 64
#define LRL_HASH_OF_TRUE   8241037574259036601UL
#define LRL_HASH_OF_FALSE 15759468864460451032UL
#else
#define LRL_HASH_BITS 32
#define LRL_HASH_OF_TRUE   3305015823UL
#define LRL_HASH_OF_FALSE  1357136097UL
#endif

/* Error reporting */
#define LRL_NULL_LOCATION { NULL, 0 }
typedef struct {
    const char *start;
    size_t length;
} LRLCodeLocation;

typedef enum {
    #define LRL_D(n, s) n,
    #include "context_errors.h"
    #undef LRL_D
    LRL_NumErrorTypes
} LRLErrorType;

typedef struct {
    const LRLASTInterop *interop;
    const char *filename; /* pointer is valid during error handler call only */
    int line, column;
} LRLInteropError;

#define LRL_MAX_ERR_LOCATIONS 3
typedef struct {
    LRLErrorType type;
    
    LRLInteropError interop; /* used if filename != NULL */
    LRLCodeLocation locs[LRL_MAX_ERR_LOCATIONS];
} LRLError;

typedef struct LRLErrorCtx {
    const struct LRLErrorCtx *parent;
    LRLCodeLocation loc;
} LRLErrorCtx;

LRLCtx *lrl_ctx_new(LRLConfig *config);
void lrl_ctx_free(LRLCtx *ctx);

LRLFileInfo *lrl_ctx_add_file(LRLCtx *ctx, char *filename, char *source);
void lrl_ctx_set_file_ast(LRLFileInfo *fileinfo, LRLASTNamespace *root);
void lrl_ctx_find_source(const LRLCtx *ctx, const char *s,
                         const char **filename, int *line, int *column);
void lrl_ctx_current_error_source(const LRLCtx *ctx, const char **filename,
                                  int *line, int *column);


/*
 * Error reporting functions
 *
 * Call lrl_err_set_*() to set the location of the error and then
 * lrl_err_finish() to report the error.
 */
void lrl_err_set_char(LRLCtx *ctx, const char *source, size_t index);
void lrl_err_set_char_range(LRLCtx *ctx, const char *source, size_t length,
                            size_t index);
void lrl_err_set_loc(LRLCtx *ctx, const LRLCodeLocation *loc, size_t index);
void lrl_err_set_token(LRLCtx *ctx, const LRLToken *token, size_t index);
void lrl_err_set_token_range(LRLCtx *ctx,
                             const LRLToken *from, const LRLToken *to,
                             size_t index);
void lrl_err_set_ident(LRLCtx *ctx, const LRLToken *first_token,
                       size_t index);
void lrl_err_set_type(LRLCtx *ctx, const LRLASTType *type, size_t index);
void lrl_err_set_typeref(LRLCtx *ctx, const LRLTypeRef *typeref,
                         size_t index);
void lrl_err_set_expr(LRLCtx *ctx, const LRLASTExpr *expr, size_t index);
void lrl_err_set_interop(LRLCtx *ctx, const char *filename, int line,
                         int column, const LRLASTInterop *interop);

void lrl_err_finish(LRLCtx *ctx, LRLErrorType err);

/* Shorthand functions for error reporting */
void lrl_err_char(LRLCtx *ctx, LRLErrorType err, const char *source);
void lrl_err_token(LRLCtx *ctx, LRLErrorType err, const LRLToken *tok);
void lrl_err_expr(LRLCtx *ctx, LRLErrorType err, const LRLASTExpr *expr);
void lrl_err_type(LRLCtx *ctx, LRLErrorType err, const LRLASTType *type);

/* Error context, for e.g. referenced and evaluated expressions */
void lrl_errctx_start_loc(LRLCtx *ctx, LRLErrorCtx *errctx,
                          const LRLToken *from, const LRLToken *to);
void lrl_errctx_end(LRLCtx *ctx, const LRLErrorCtx *errctx);

/* Checking if there is at least one error */
int lrl_ctx_has_errors(const LRLCtx *ctx);
void lrl_ctx_set_has_errors(LRLCtx *ctx, int value);

/* Error handlers */
typedef void LRLErrorHandler(LRLCtx *ctx, const LRLError *err);
LRLErrorHandler *lrl_err_get_handler(LRLCtx *ctx);
void lrl_err_set_handler(LRLCtx *ctx, LRLErrorHandler *handler);
void lrl_err_default_handler(LRLCtx *ctx, const LRLError *err);

/* Binds new/unbound identifiers */
void lrl_ctx_bind_verify(LRLCtx *ctx);
void lrl_ctx_set_has_deferred(LRLCtx *ctx);
void lrl_ctx_set_has_completed_constexpr(LRLCtx *ctx);

#endif

