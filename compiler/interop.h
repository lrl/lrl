/*

  interop.h -- Interface for language interopability, e.g. for C headers

  Copyright © 2013-2014 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#ifndef LRL_INTEROP_H
#define LRL_INTEROP_H

#include "context.h"
#include "parser.h"

/*

// transformed into a namespace
interop here = "c" [
    :include(
        :system,
        "event2/event.h",
        [
            :translate("event_", "event2:"),
            :rewrite_namespace("event_", "event2");
        ]),
];


// This should simply include everything:
//   - functions/variables/types(structs/enums/unions).
//     pointers and variables use "unknown_var" and "unknown_own" qualifiers.
//   - names don't use namespaces (by default)
//   - ifdefs etc. could be handled by cpp
//   - "#define C V" is replaced with "alias T C = V" if T can be deduced.
//   - "#define M(X) EXPR" is replaced with "alias TR M(TX X) { return EXPR; }"
//     if TR and TX can be detected.
//   - Use "cpp -dD" to include both #defines and the processed output
interop here = "c" [:include(:system, "event2.h", [])];


*/

struct LRLInteropImpl {
    /* Information */
    const char *name;
    
    /* Functions */
    int (*is_supported)(const char *name, size_t namelen);
    const LRLASTType *(*get_options_type)(LRLCtx *ctx);
    int (*translate)(LRLCtx *ctx, LRLASTInterop *interop_ast);
};

/* Set defaults */
#if !defined(ENABLE_INTEROP_C_BUILTIN)
#define ENABLE_INTEROP_C_BUILTIN 1
#endif

#define LRL_INTEROP_COUNT  (0+(ENABLE_INTEROP_C_BUILTIN))
const LRLInteropImpl *lrl_interop_lookup(const char *name, size_t namelen);


#endif

