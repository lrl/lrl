/*

  tests.h -- Declarations for unit tests

  Copyright © 2016 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/
#ifndef UNITTEST_H
#define UNITTEST_H

typedef void TestFunction(void);
typedef struct {
    const char *name;
    TestFunction *function;
} TestFunctionInfo;


/* Debugging and assertion functions */
void tprintf(const char *format, ...);
void tfail_(const char *filename, int line, const char *message);
int tsoftfail_(const char *filename, int line, const char *message, int ok);
#define tassert(expr) if (!(expr)) { tfail_(__FILE__, __LINE__, #expr); }
#define tsoftassert(expr) tsoftfail_(__FILE__, __LINE__, #expr, expr)
#define tfail(message) tfail_(__FILE__, __LINE__, message)
#define tsoftfail(message) tsoftfail_(__FILE__, __LINE__, message)

/* Test function info in test source files */
#define TEST_INFO(name) { #name, &name },
#define TEST_END { NULL, NULL }

/* Called from test_all() below */
void test_file(const char *filename, const TestFunctionInfo *tests);

/* Declare all lists of unit tests, and a function to run them */
#define TEST_D(name) extern const TestFunctionInfo tests_##name[];
#include "alltests.h"
#undef TEST_D
#ifdef TEST_MAIN
static void test_all(void)
{
#define TEST_D(name) test_file(#name ".c", tests_##name);
#include "alltests.h"
#undef TEST_D
}
#endif

#endif

