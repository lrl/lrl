/*

  test_misc.c -- Tests for misc.c

  Copyright © 2017 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include "../misc.c"
#include "unittest.h"

static void test_linprobset_expand(void)
{
    LinProbSet lps;
    lps.size = 1;
    lps.set = malloc(2*sizeof(LRLHashCode));
    lps.set[0] = 0x122;
    lps.set[1] = 0x341;
    linprobset_expand(&lps);
    tsoftassert(lps.size == 2);
    tsoftassert(lps.set[0] == 0);
    tsoftassert(lps.set[1] == 0x341);
    tsoftassert(lps.set[2] == 0x122);
    tsoftassert(lps.set[3] == 0);
    
    lps.size = 1;
    lps.set = realloc(lps.set, 2*sizeof(LRLHashCode));
    lps.set[0] = 0;
    lps.set[1] = 0x321;
    linprobset_expand(&lps);
    tsoftassert(lps.size == 2);
    tsoftassert(lps.set[0] == 0);
    tsoftassert(lps.set[1] == 0x321);
    tsoftassert(lps.set[2] == 0);
    tsoftassert(lps.set[3] == 0);
    
    lps.size = 1;
    lps.set = realloc(lps.set, 2*sizeof(LRLHashCode));
    lps.set[0] = 0x451;
    lps.set[1] = 0x321;
    linprobset_expand(&lps);
    tsoftassert(lps.size == 2);
    tsoftassert(lps.set[0] == 0);
    tsoftassert(lps.set[1] == 0x321);
    tsoftassert(lps.set[2] == 0x451);
    tsoftassert(lps.set[3] == 0);
}

static void test_linprobset_add(void)
{
    LinProbSet lps = { 123, (void*)0x123 };
    size_t i;
    
    linprobset_init(&lps, 1);
    tsoftassert(lps.set != NULL);
    tsoftassert(lps.size == 1);
    tsoftassert(lps.set[0] == 0);
    tsoftassert(lps.set[1] == 0);
    
    tsoftassert(linprobset_add_hashcode(&lps, 0x321) == 0);
    tsoftassert(lps.set[0] == 0);
    tsoftassert(lps.set[1] == 0x321);
    tsoftassert(lps.size == 1);
    tsoftassert(linprobset_add_hashcode(&lps, 0x321) == 1);
    tsoftassert(lps.set[0] == 0);
    
    /* Collides and grows */
    tsoftassert(linprobset_add_hashcode(&lps, 0x451) == 0);
    tsoftassert(lps.size == 2);
    tsoftassert(lps.set[0] == 0);
    tsoftassert(lps.set[1] == 0x321);
    tsoftassert(lps.set[2] == 0x451);
    tsoftassert(lps.set[3] == 0);
    
    /* Direct add */
    tsoftassert(linprobset_add_hashcode(&lps, 0x674) == 0);
    tsoftassert(lps.size == 2);
    tsoftassert(lps.set[0] == 0x674);
    tsoftassert(lps.set[1] == 0x321);
    tsoftassert(lps.set[2] == 0x451);
    tsoftassert(lps.set[3] == 0);
    tsoftassert(linprobset_add_hashcode(&lps, 0x674) == 1);
    tsoftassert(lps.set[0] == 0x674);
    
    /* Collides and grows */
    tsoftassert(linprobset_add_hashcode(&lps, 0x894) == 0);
    tsoftassert(lps.size == 3);
    tsoftassert(lps.set[0] == 0);
    tsoftassert(lps.set[1] == 0x321);
    tsoftassert(lps.set[2] == 0x451);
    tsoftassert(lps.set[3] == 0);
    tsoftassert(lps.set[4] == 0x674);
    tsoftassert(lps.set[5] == 0x894);
    tsoftassert(lps.set[6] == 0);
    tsoftassert(lps.set[7] == 0);
    tsoftassert(linprobset_add_hashcode(&lps, 0x451) == 1);
    tsoftassert(lps.set[0] == 0);
    
    /* Collides */
    tsoftassert(linprobset_add_hashcode(&lps, 0x111) == 0);
    tsoftassert(lps.size == 3);
    tsoftassert(lps.set[0] == 0);
    tsoftassert(lps.set[1] == 0x321);
    tsoftassert(lps.set[2] == 0x451);
    tsoftassert(lps.set[3] == 0x111);
    tsoftassert(lps.set[4] == 0x674);
    tsoftassert(lps.set[5] == 0x894);
    tsoftassert(lps.set[6] == 0);
    tsoftassert(lps.set[7] == 0);
    tsoftassert(linprobset_add_hashcode(&lps, 0x111) == 1);
    tsoftassert(lps.set[0] == 0);
    
    /* Collides and grows */
    tsoftassert(linprobset_add_hashcode(&lps, 0x221) == 0);
    tsoftassert(lps.size == 4);
    tsoftassert(lps.set[0] == 0);
    tsoftassert(lps.set[1] == 0x321);
    tsoftassert(lps.set[2] == 0x451);
    tsoftassert(lps.set[3] == 0x111);
    tsoftassert(lps.set[4] == 0x674);
    tsoftassert(lps.set[5] == 0x894);
    tsoftassert(lps.set[6] == 0x221);
    tsoftassert(lps.set[7] == 0);
    tsoftassert(linprobset_add_hashcode(&lps, 0x221) == 1);
    tsoftassert(lps.set[0] == 0);
    
    tsoftassert(linprobset_add_hashcode(&lps, 0x101) == 0);
    tsoftassert(lps.set[7] == 0x101);
    
    /* Collides and wraps around */
    tsoftassert(linprobset_add_hashcode(&lps, 0x55f) == 0);
    tsoftassert(lps.size == 4);
    tsoftassert(lps.set[0xf] == 0x55f);
    tsoftassert(linprobset_add_hashcode(&lps, 0x66f) == 0);
    tsoftassert(lps.size == 4);
    tsoftassert(lps.set[0xf] == 0x55f);
    tsoftassert(lps.set[0] == 0x66f);
    
    /* Test with no collisions */
    free(lps.set);
    linprobset_init(&lps, 1);
    for (i = 1; i < 100; i++) {
        tsoftassert(linprobset_add_hashcode(&lps, i) == 0);
    }
    
    /* Test with pseudo-random data */
    free(lps.set);
    linprobset_init(&lps, 1);
    for (i = 1; i < 1000; i++) {
        tsoftassert(linprobset_add_hashcode(&lps, 1234567*i) == 0);
    }
}

const TestFunctionInfo tests_misc[] = {
    TEST_INFO(test_linprobset_expand)
    TEST_INFO(test_linprobset_add)
    TEST_END
};


