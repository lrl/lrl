/*

  test_constexpr.c -- Tests for constexpr.c

  Copyright © 2016 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include "../constexpr.c"
#include "unittest.h"

static Number na, nb, nr;

static void set_number(Number *num, const char *s)
{
    int is_neg = s[0] == '-';
    if (is_neg) { s++; }
    num->loc.start = s;
    num->loc.length = strlen(s);
    num->is_neg = is_neg;
}

#define assert_num(np, s) assert_num_(__FILE__, __LINE__, np, s)
static int assert_num_(const char *file, int line,
                        const Number *np, const char *s)
{
    int slen, nlen;
    int is_neg = s[0] == '-';
    if (is_neg) { s++; }

    nlen = size2int(np->loc.length);
    slen = strlen(s);

    if (nlen != slen ||
        strncmp(np->loc.start, s, slen) ||
        np->is_neg != is_neg) {
        char message[250];
        if (nlen > 100) nlen = 100;
        
        sprintf(message, "expected %s%s, actual %s%.*s",
                is_neg ? "-" : "", s,
                np->is_neg ? "-" : "", nlen, np->loc.start);
        return tsoftfail_(file, line, message, 0);
    }
    return 1;
}

#define TEST_TOHEX(dec, hex) do { \
        set_number(&na, (dec)); \
        (void)(tsoftassert(to_hex(&na, &nr)) && \
            assert_num(&nr, (hex))); \
    } while (0)
static void test_tohex(void)
{
    TEST_TOHEX("0", "0x0");
    TEST_TOHEX("16", "0x10");
    TEST_TOHEX("123", "0x7b");
    TEST_TOHEX("987654321", "0x3ade68b1");
    TEST_TOHEX("314159265358979323846264338327950",
               "0xf7d3f558f10c5dfd556e3e5298e");
    TEST_TOHEX("3_141_592_653_589_793_238_462_643_383_279_50",
               "0xf7d3f558f10c5dfd556e3e5298e");
    TEST_TOHEX("340282366920938463463374607431768211455", /* 2^128 */
               "0xffffffffffffffffffffffffffffffff");
}

#define TEST_TOINT(dec, max, res) do { \
        unsigned uir; \
        set_number(&na, (dec)); \
        (void)(tsoftassert(to_int(&na, &uir, max)) && \
            tsoftassert(uir == (res))); \
    } while (0)
static void test_toint(void)
{
    TEST_TOINT("0",          100,       0);
    TEST_TOINT("16",         100,      16);
    TEST_TOINT("123",        100,     100);
    TEST_TOINT("12345",    30000,   12345);
    TEST_TOINT("12345",      123,     123);
    TEST_TOINT("0x3ade68b1", 123,     123);
    TEST_TOINT("340282366920938463463374607431768211455", /* 2^128 */
               123, 123);
}

#define TEST_ADD(a, b, op, res) do { \
        set_number(&na, (a)); set_number(&nb, (b)); \
        (void)(tsoftassert(calc_add(LRL_Op_##op, &na, &nb, &nr)) && \
            assert_num(&nr, (res))); \
    } while(0)
static void test_add(void)
{
    TEST_ADD("0", "0", Plus, "0");
    TEST_ADD("0", "0x0", Plus, "0x0");
    TEST_ADD("0x19", "3", Plus, "0x1c");
    TEST_ADD("11", "0xfe", Plus, "0x109");
    TEST_ADD("0x109", "11", Minus, "0xfe");
    TEST_ADD("16", "0x02_0", Minus, "-0x10");
    TEST_ADD("16", "-0x02_0", Plus, "-0x10");
    TEST_ADD("-9999", "-7654", Plus, "-17653");
    TEST_ADD("-9999", "-0x0_00a", Plus, "-0x2719");
}

#define TEST_MUL(a, b, res) do { \
        set_number(&na, (a)); set_number(&nb, (b)); \
        (void)(tsoftassert(calc_mul(&na, &nb, &nr)) && \
            assert_num(&nr, (res))); \
    } while(0)
static void test_mul(void)
{
    TEST_MUL("0", "0", "0");
    TEST_MUL("0", "0x0", "0x0");
    TEST_MUL("0x19", "3", "0x4b");
    TEST_MUL("11", "0xfe", "0xaea");
    TEST_MUL("0x109", "11", "0xb63");
    TEST_MUL("16", "0x02_0", "0x200");
    TEST_MUL("16", "-0x02_0", "-0x200");
    TEST_MUL("-9999", "-7654", "76532346");
    TEST_MUL("-9999", "-0x0_00a", "0x18696");
    TEST_MUL("4679", "-6", "-28074");
    TEST_MUL("0xFF", "99", "0x629d");
    TEST_MUL("0xFF", "0x7F", "0x7e81");
    TEST_MUL("340282366920938463463374607431768211455", "340282366920938463463374607431768211455", "115792089237316195423570985008687907852589419931798687112530834793049593217025");
}

#define TEST_COMPARE(a, b, res) do { \
        int cmp; \
        set_number(&na, (a)); set_number(&nb, (b)); \
        cmp = calc_compare(&na, &nb, 0); \
        tsoftassert(cmp == (res)); \
    } while(0)
static void test_compare(void)
{
    TEST_COMPARE(    "0",    "0",  0);
    TEST_COMPARE(    "0",  "0x0",  0);
    TEST_COMPARE(    "0",    "1", -1);
    TEST_COMPARE(    "0",  "0x1", -1);
    TEST_COMPARE(    "1",    "0",  1);
    TEST_COMPARE(  "0x1",    "0",  1);
    TEST_COMPARE("0x012",  "0_9",  1);
    TEST_COMPARE("0x012",  "1_8",  0);
    TEST_COMPARE("0x012", "019_", -1);
    TEST_COMPARE("0x129","0x12a", -1);
    TEST_COMPARE("0x129","0x12A", -1);
    TEST_COMPARE("0x192","0x1a2", -1);
}

#define TEST_BITWISE(a, b, op, res) do { \
        set_number(&na, (a)); set_number(&nb, (b)); \
        calc_bitwise(&na, &nb, &nr, LRL_Op_Bit##op); \
        assert_num(&nr, (res)); \
    } while(0)
static void test_bitwise(void)
{
    TEST_BITWISE("0x0", "0x0", Xor, "0x0");
    TEST_BITWISE("0x3", "0x1", Xor, "0x2");
    TEST_BITWISE("0xf7d3f558f10c5dfd556e3e5298e",
                 "0xcc8034262683b6a11cf697be1c1adc2f", Xor,
                 "0xcc803b5b19d639b1d92942e8fffff5a1");
    TEST_BITWISE("0xf7_d3f55_8f10_c5df_d556_e3e_5298e",
                 "0xcc8034262683b6a11cf6_97be1c1a_dc2f_", And,
                 "0x000004242601860004d695160000080e");
    TEST_BITWISE("0xf000000000000000000000",
                 "0x______________1_______", Or,
                 "0xf000000000000000000001");
}

#define TEST_SHIFT(a, b, op, res) do { \
        set_number(&na, (a)); set_number(&nb, (b)); \
        calc_shift(&na, &nb, &nr, LRL_Op_Shift##op); \
        assert_num(&nr, (res)); \
    } while(0)
static void test_shift(void)
{
    TEST_SHIFT("0x0",  "0", L,    "0x0");
    TEST_SHIFT("0x0",  "0", R,    "0x0");
    TEST_SHIFT("0x0",  "1", R,    "0x0");
    TEST_SHIFT("0x0",  "1", R,    "0x0");
    TEST_SHIFT("0x_0", "1", R,    "0x0");
    TEST_SHIFT("0x0_", "1", R,    "0x0");
    TEST_SHIFT("1",    "1", R,    "0x0");
    TEST_SHIFT("0x001","1", R,    "0x0");
    TEST_SHIFT("0x0_1","1", R,    "0x0");
    TEST_SHIFT("0x_01","1", R,    "0x0");
    TEST_SHIFT("0x01_","1", R,    "0x0");
    TEST_SHIFT("0x3",  "1", L,    "0x6");
    TEST_SHIFT("0x3",  "1", R,    "0x1");
    TEST_SHIFT("0x9",  "1", L,   "0x12");
    TEST_SHIFT("0x12", "1", R,    "0x9");
    TEST_SHIFT("0x12", "1", R,    "0x9");
    TEST_SHIFT("0x9",  "9", L, "0x1200");
    TEST_SHIFT("0x1200", "9", R,  "0x9");
    TEST_SHIFT("0xff", "128", R,  "0x0");
    TEST_SHIFT("0xff_", "128", R, "0x0");
    TEST_SHIFT("0xf_f", "128", R, "0x0");
    TEST_SHIFT("0x_ff", "128", R, "0x0");
    TEST_SHIFT("0xf7d3f558f10c5dfd556e3e5298e", "3", L,
               "0x7be9faac78862efeaab71f294c70");
    TEST_SHIFT("0xf7d3f558f10c5dfd_556_e3e_5298e", "20", L,
               "0xf7d3f558f10c5dfd556e3e5298e00000");
    TEST_SHIFT("0xcc8034262683b6a11cf6_97be1c1a_dc2f_", "3", R,
               "0x19900684c4d076d4239ed2f7c3835b85");
    TEST_SHIFT("0xf000000abcdef00000010f", "7", R,
               "0x1e000001579bde0000002");
    TEST_SHIFT("0x111222333aaabbbccc9876", "7", L,
               "0x88911199d555dde664c3b00");
}


const TestFunctionInfo tests_constexpr[] = {
    TEST_INFO(test_tohex)
    TEST_INFO(test_toint)
    TEST_INFO(test_add)
    TEST_INFO(test_mul)
    TEST_INFO(test_compare)
    TEST_INFO(test_bitwise)
    TEST_INFO(test_shift)
    TEST_END
};

