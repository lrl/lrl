/*

  testmain.c -- Main entry point for unit tests

  Copyright © 2016 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <setjmp.h>

#define TEST_MAIN
#include "unittest.h"

static const char *current_file;
static const char *current_function;
static const TestFunctionInfo *fi;
static jmp_buf failjump;

int test_errors = 0;
int quiet;

void test_file(const char *filename, const TestFunctionInfo *tests)
{
    current_file = filename;
    for (fi = tests; fi->name != NULL; fi++) {
        current_function = fi->name;
        if (setjmp(failjump) == 0) {
            fi->function();
        }
    }
}

void tprintf(const char *format, ...)
{
    va_list ap;
    
    va_start(ap, format);
    vfprintf(stderr, format, ap);
    va_end(ap);
}

void tfail_(const char *filename, int line, const char *message)
{
    tsoftfail_(filename, line, message, 0);
    longjmp(failjump, 1);
}

int tsoftfail_(const char *filename, int line, const char *message, int ok)
{
    if (ok) return 1;
    test_errors++;
    if (!quiet) {
        fprintf(stderr, "TEST FAILURE: %s:%d: failed assertion: %s\n",
                filename, line, message);
    }
    return 0;
}

int main(void)
{
    test_all();
    return test_errors ? 1 : 0;
}


