/*

  configfile.c -- Loads configuration files

  Copyright © 2013-2016 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include "configfile.h"
#include "identifier.h"
#include "misc.h"
#include "parser.h"
#include "platform.h"

#include <dirent.h>
#include <stdio.h>
#include <string.h>

struct LRLConfig {
    /* TODO keep a list of file source, so it can be free'd */
    LRLIdent root;
};

static void config_set(LRLConfig *conf, char *section, char *name, char *value);


LRLConfig *lrl_config_new(void)
{
    return calloc(1, sizeof(LRLConfig));
}

static void cfgerror(const char *filename,
                     const char *start, const char *p,
                     const char *message)
{
    int line = 1;
    while (start < p) {
        char c = *(start++);
        if (c == '\n') {
            line++;
        } else if (c == '\r') {
            line++;
            if (*start == '\n') start++;
        }
    }
    
    fprintf(stderr, "%s:%d: %s\n", filename, line, message);
}

int lrl_config_load_file(LRLConfig *conf, const char *filename)
{
    FILE *file;
    char *start, *p;
    enum { Outside, Comment, Section, Name, Value } state;
    char *namestart = NULL, *valuestart = NULL, *section = NULL;
    
    /* Load file */
    file = fopen(filename, "rb");
    if (!file) {
        perror(filename);
        return 0;
    }
    start = file_get_contents(file);
    fclose(file);
    if (!start) {
        perror(filename);
        return 0;
    }
    
    /* Parse it */
    state = Outside;
    p = start;
    for (;;) {
        if (*p == '#') state = Comment;
        
        switch (state) {
            case Outside:
                while (*p > 0 && *p <= ' ') p++;
                if (!*p) goto end;
                if (*p == '[') state = Section;
                else state = Name;
                break;
            case Comment:
                p += strcspn(p, "\r\n");
                state = Outside;
                break;
            case Section: {
                p++;
                section = p;
                for (;;) {
                    if (*p == '\n' || *p == '\r' || !*p || *p == '#') {
                        cfgerror(filename, start, p, "Expected a \"]\"");
                        state = (*p == '#' ? Comment : Outside);
                        break;
                    } else if (*p == ']') {
                        state = Outside;
                        break;
                    }
                    p++;
                }
                
                *p = '\0'; /* newline or ] */
                p++;
                
                if ((*p < 0 || *p > ' ') && *p != '#') {
                    cfgerror(filename, start, p, "Expected a newline");
                }
                
                break; }
            case Name:
                namestart = p;
                for (;;) {
                    if (*p == '\n' || *p == '\r' || !*p || *p == '#') {
                        cfgerror(filename, start, p, "Expected a \"=\"");
                        state = (*p == '#' ? Comment : Outside);
                        break;
                    } else if (*p > 0 && *p <= ' ') {
                        *p = '\0';
                    } else if (*p == '=') {
                        if (p == namestart) {
                            cfgerror(filename, start, p, "Expected a name before \"=\"");
                        }
                        *p = '\0';
                        p++;
                        state = Value;
                        break;
                    }
                    p++;
                }
                break;
            case Value:
                /* Skip leading whitespace */
                while (*p > 0 && *p <= ' ' && *p != '\n' && *p != '\r') {
                    *p = '\0';
                    p++;
                }
                
                valuestart = p;
                while (*p && *p != '\n' && *p != '\r' && *p != '#') p++;
                state = (*p == '#' ? Comment : Outside);
                *(p++) = '\0';
                
                config_set(conf, section, namestart, valuestart);
                break;
        }
    }
    
  end:
    return 1;
}

static int cmpstrptr(const void *a, const void *b)
{
    return strcmp(*(const char* const*)a, *(const char* const*)b);
}

int lrl_config_load_dir(LRLConfig *conf, const char *dirname)
{
    char **sorted_entry;
    char **entries;
    size_t entry_count, entry_capa;
    struct dirent *ent;
    DIR *dir = opendir(dirname);
    if (!dir) return 0;
    
    /* Read all entries to an array that can be sorted */
    init_list(&entries, &entry_count, &entry_capa, 4);
    while ((ent = readdir(dir)) != NULL) {
        char *filename;
        size_t namelen;
        
        /* Skip hidden files */
        if (ent->d_name[0] == '.') continue;
        
        /* Skip non *.conf files */
        namelen = strlen(ent->d_name);
        if (namelen < 5 || strcmp(ent->d_name+namelen-5, ".conf") != 0)
            continue;
        
        filename = lrl_strdup(ent->d_name);
        list_push(&entries, &entry_count, &entry_capa, filename);
    }
    closedir(dir);
    
    qsort(entries, entry_count, sizeof(char*), cmpstrptr);
    
    for (sorted_entry = entries; entry_count; entry_count--, sorted_entry++) {
        char *filename = join_paths(dirname, *sorted_entry);
        free(*sorted_entry);
        
        lrl_config_load_file(conf, filename);
        free(filename);
    }
    
    free(entries);
    return 1;
}


void lrl_config_load_system(LRLConfig *conf)
{
    /* Load dirs in reverse order */
    const LRLPath *(reversed[10]);
    const LRLPath *entry = lrl_platform_config;
    int i;
    
    for (i = 0; entry; i++) {
        if (i >= 10) fail("conffile_toomanydirs");
        reversed[i] = entry;
        entry = entry->next;
    }
    
    while (--i >= 0) {
        lrl_config_load_dir(conf, reversed[i]->path);
    }
}


const char *lrl_config_get(LRLConfig *conf, const char *section,
                           const char *name, const char *defaultval)
{
    LRLIdent *ident = &conf->root;
    char *val;
    
    if (section) {
        ident = lrl_ident_get_string(ident, section);
        if (!ident) return defaultval;
    }
    
    ident = lrl_ident_get_string(ident, name);
    if (!ident) return defaultval;
    
    val = ident->def_node->def.kind.configvalue;
    return val ? val : defaultval;
}


/* takes ownership of <value> */
static void config_set(LRLConfig *conf, char *section, char *name, char *value)
{
    LRLIdent *ident = &conf->root;
    
    if (section) {
        ident = lrl_ident_insert_string(NULL, ident, section);
    }
    
    ident = lrl_ident_insert_string(NULL, ident, name);
    
    /* create or overwrite the value */
    if (!ident->def_node) {
        ident->def_node = calloc(1, sizeof(LRLASTDef));
    }
    ident->def_node->def.kind.configvalue = value;
}


void lrl_config_free(LRLConfig *conf)
{
    /* TODO free identifier tree and file data */
    free(conf);
}



