/*

  context_private.h -- Private types for the compilation context.

  Copyright © 2011-2016 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#ifndef LRL_CONTEXT_PRIVATE_H
#define LRL_CONTEXT_PRIVATE_H

#include "tokenizer.h"
#include "parser.h"
#include "identifier.h"

struct LRLFileInfo {
    LRLFileInfo *next;
    
    /** Whether the verify phase has completed (identifiers are bound, etc) */
    int completed;
    
    /** Source file being processed */
    const char *filename;
    const char *source;
    
    /** Root node in AST */
    LRLASTNamespace *root;
};

struct LRLCtx {
    LRLFileInfo *files;
    
    /* Used for building information for the next error */
    LRLError error;
    /* Error handler function (default one prints to stderr) */
    LRLErrorHandler *error_handler;
    /* Error context, for e.g. referenced and evaluated expressions */
    const LRLErrorCtx *errctx;
    
    /* Deferred identifier lookups */
    LRLIdentRef *deferred_list;
    LRLIdentRef *uses_list;
    LRLASTInterop *interop_list;
    int has_deferred;
    int has_parsed; /* to detect cyclic identifier references */
    int has_completed_constexpr; /* at least one constepr has been evaluated */
    int allow_local_type_params; /* whetever type parameters from the current
                                    toplevel namespace may be used. */
    
    /* Root of everything containing builtin identifiers, e.g. "int" */
    LRLIdent *builtins_scope;
    
    /* Used to cache interop types etc. */
    LRLIdent *internals_scope;
    
    /* Flat namespace of all linknames. Used by interop statements
       to avoid creating duplicates */
    LRLIdent *external_scope;
    
    /* Increment to disable searching of identifiers from the filesystem */
    int no_filesystem;
    
    /* Whether at least one error has occurred */
    int has_errors;
    
    /* Const expr error handling and cycle detection */
    const LRLASTExpr *constexpr_errorexpr;
    const LRLASTExpr *constexpr_cyclestart;
    int constexpr_iteration;
    int constexpr_nextstop;
    
    /* Dynamically created builtins */
    /* TODO move stuff with pointers from builtins.c to here */
    LRLTypeRef bool_tr;
    
    LRLConfig *config;
};

#endif


