LRL Documentation
=================

This directory contains documentation for LRL. All documents are in Markdown
format, but can also be viewed in a text editor. You can also use your
favorite search utility (e.g. **grep** on Unix-like systems) to search in the
documentation. To search in all text, you can use:

    grep -R 'text editor' .

To search only in headers you can use:

    grep -R '^while$' .
    
If you read the documentation in a text editor, please note that underscores
may sometimes be escaped with a backslash before them, e.g:

    like\_this, which should be read like_this

Where to go next?
-----------------

**TODO:** Actually write the intro.md and the syslib manual.

The file **intro.md** contains a quick-start introduction. In the directory
**language\_manual** the full language is described, while in the directory
**syslib\_manual** descibes the built-in system library. The directory
**notes** contains some notes and ideas for the language, but these may not
be up to date and/or might not be implemented at all in LRL (and might never
be).

