Tips and Tricks
===============

Default values for structs
--------------------------
LRL does not (and will not) have default values for structs built in, but
you can do something similar by combining multiple assignment and a typescope:

    typedef Person = (
        byte#[undefined]+> name,  // FIXME assigning to this array does not work due to it having an unknown length
        int age,
    );
    Person Person:defval = (@"John Doe", 40);

Then you can use it:

    var Person jane = :defval;
    jane.name = @"Jane Doe";

    // The result is (@"Jane Doe", 40);


