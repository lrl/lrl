Types
=====

Type compatibility
------------------
When LRL checks type compatibility, it will first check if both types have
a name (and neither type is an alias). If so, the types are considered equal
only if the names match. Otherwise, the structure of the types will be
compared (e.g. it will be possible to assign an "int" to a "long", or two
equals structs to each other).

**TODO** Test aliases more thorougly
**NOTE** should not be allowed to "cast" pointers from one named type to
another through an unnamed structurally compatible type. perhaps change the
rules to allow only casts of pointers in one direction? or not at all?

Type qualifiers
---------------
**TODO**

Identifier types
----------------
**TODO**

Builtin types
-------------
There are several numeric types and a bool type.

**NOTE** explain kinds of types, wrap around, etc. special types byte/uint8, eint, wuint/uint compatibility...
**NOTE** short<=int<=long<=longlong
**NOTE** float<=double etc.
**NOTE** should byte be compatible with eint8?
**NOTE** floating point formats have an implied bit in the coefficient

<table>
<tr><th>Name</th><th>Kind</th><th>Signed?</th><th>Wraps?</th><th>Min Bits</th><th>Max Bits</th></tr>
<tr><td>count     </td><td>Integer - word-sized </td><td>&nbsp;</td><td>&nbsp;</td><td>16</td><td>&infin;</td></tr>
<tr><td>wcount    </td><td>Integer - word-sized </td><td>&nbsp;</td><td>Yes   </td><td>16</td><td>&infin;</td></tr>

<tr><td>uint      </td><td>Integer - system size</td><td>&nbsp;</td><td>&nbsp;</td><td>16</td><td>&infin;</td></tr>
<tr><td>wuint     </td><td>Integer - system size</td><td>&nbsp;</td><td>&nbsp;</td><td>16</td><td>&infin;</td></tr>
<tr><td>int       </td><td>Integer - system size</td><td>Yes   </td><td>&nbsp;</td><td>15+1</td><td>&infin;</td></tr>
<tr><td>eint      </td><td>Integer - system size</td><td>&nbsp;</td><td>&nbsp;</td><td>15+1</td><td>&infin;</td></tr>

<tr><td>ushort    </td><td>Integer - system size</td><td>&nbsp;</td><td>&nbsp;</td><td>16</td><td>&infin;</td></tr>
<tr><td>wushort   </td><td>Integer - system size</td><td>&nbsp;</td><td>&nbsp;</td><td>16</td><td>&infin;</td></tr>
<tr><td>short     </td><td>Integer - system size</td><td>Yes   </td><td>&nbsp;</td><td>15+1</td><td>&infin;</td></tr>
<tr><td>eshort    </td><td>Integer - system size</td><td>&nbsp;</td><td>&nbsp;</td><td>15+1</td><td>&infin;</td></tr>

<tr><td>ulong     </td><td>Integer - system size</td><td>&nbsp;</td><td>&nbsp;</td><td>32</td><td>&infin;</td></tr>
<tr><td>wulong    </td><td>Integer - system size</td><td>&nbsp;</td><td>&nbsp;</td><td>32</td><td>&infin;</td></tr>
<tr><td>long      </td><td>Integer - system size</td><td>Yes   </td><td>&nbsp;</td><td>31+1</td><td>&infin;</td></tr>
<tr><td>elong     </td><td>Integer - system size</td><td>&nbsp;</td><td>&nbsp;</td><td>31+1</td><td>&infin;</td></tr>

<tr><td>ulonglong </td><td>Integer - system size</td><td>&nbsp;</td><td>&nbsp;</td><td>64</td><td>&infin;</td></tr>
<tr><td>wulonglong</td><td>Integer - system size</td><td>&nbsp;</td><td>&nbsp;</td><td>64</td><td>&infin;</td></tr>
<tr><td>longlong  </td><td>Integer - system size</td><td>Yes   </td><td>&nbsp;</td><td>63+1</td><td>&infin;</td></tr>
<tr><td>elonglong </td><td>Integer - system size</td><td>&nbsp;</td><td>&nbsp;</td><td>63+1</td><td>&infin;</td></tr>

<tr><td>byte      </td><td>Integer - fixed size </td><td>&nbsp;</td><td>&nbsp;</td><td> 8</td><td>8</td></tr>
<tr><td>char      </td><td>Integer - fixed size </td><td>&nbsp;</td><td>&nbsp;</td><td> 8</td><td>8</td></tr>
<tr><td>uint8     </td><td>Integer - fixed size </td><td>&nbsp;</td><td>&nbsp;</td><td> 8</td><td>8</td></tr>
<tr><td>wuint8    </td><td>Integer - fixed size </td><td>&nbsp;</td><td>Yes   </td><td> 8</td><td>8</td></tr>
<tr><td>int8      </td><td>Integer - fixed size </td><td>Yes   </td><td>&nbsp;</td><td> 7+1</td><td>7+1</td></tr>
<tr><td>eint8     </td><td>Integer - fixed size </td><td>&nbsp;</td><td>&nbsp;</td><td> 7+1</td><td>7+1</td></tr>

<tr><td>uint16    </td><td>Integer - fixed size </td><td>&nbsp;</td><td>&nbsp;</td><td>16</td><td>16</td></tr>
<tr><td>wuint16   </td><td>Integer - fixed size </td><td>&nbsp;</td><td>Yes   </td><td>16</td><td>16</td></tr>
<tr><td>int16     </td><td>Integer - fixed size </td><td>Yes   </td><td>&nbsp;</td><td>15+1</td><td>15+1</td></tr>
<tr><td>eint16    </td><td>Integer - fixed size </td><td>&nbsp;</td><td>&nbsp;</td><td>15+1</td><td>15+1</td></tr>

<tr><td>uint32    </td><td>Integer - fixed size </td><td>&nbsp;</td><td>&nbsp;</td><td>32</td><td>32</td></tr>
<tr><td>wuint32   </td><td>Integer - fixed size </td><td>&nbsp;</td><td>Yes   </td><td>32</td><td>32</td></tr>
<tr><td>int32     </td><td>Integer - fixed size </td><td>Yes   </td><td>&nbsp;</td><td>31+1</td><td>31+1</td></tr>
<tr><td>eint32    </td><td>Integer - fixed size </td><td>&nbsp;</td><td>&nbsp;</td><td>31+1</td><td>31+1</td></tr>

<tr><td>uint64    </td><td>Integer - fixed size </td><td>&nbsp;</td><td>&nbsp;</td><td>64</td><td>64</td></tr>
<tr><td>wuint64   </td><td>Integer - fixed size </td><td>&nbsp;</td><td>Yes   </td><td>64</td><td>64</td></tr>
<tr><td>int64     </td><td>Integer - fixed size </td><td>Yes   </td><td>&nbsp;</td><td>63+1</td><td>63+1</td></tr>
<tr><td>eint64    </td><td>Integer - fixed size </td><td>&nbsp;</td><td>&nbsp;</td><td>63+1</td><td>63+1</td></tr>

<tr><td>uint128   </td><td>Integer - fixed size </td><td>&nbsp;</td><td>&nbsp;</td><td>128</td><td>128</td></tr>
<tr><td>wuint128  </td><td>Integer - fixed size </td><td>&nbsp;</td><td>Yes   </td><td>128</td><td>128</td></tr>
<tr><td>int128    </td><td>Integer - fixed size </td><td>Yes   </td><td>&nbsp;</td><td>127+1</td><td>127+1</td></tr>
<tr><td>eint128   </td><td>Integer - fixed size </td><td>&nbsp;</td><td>&nbsp;</td><td>127+1</td><td>127+1</td></tr>

<tr><td>float     </td><td>Floating point - system size</td><td>Yes  </td><td>&nbsp;</td><td>32(1+23+8)</td><td>&infin;</td></tr>
<tr><td>float16   </td><td>Floating point - fixed size </td><td>Yes  </td><td>&nbsp;</td><td>16(1+10+5)</td><td>16</td></tr>
<tr><td>float32   </td><td>Floating point - fixed size </td><td>Yes  </td><td>&nbsp;</td><td>32(1+23+8)</td><td>32</td></tr>
<tr><td>float64   </td><td>Floating point - fixed size </td><td>Yes  </td><td>&nbsp;</td><td>64(1+52+11)</td><td>64</td></tr>
<tr><td>float80   </td><td>Floating point - fixed size </td><td>Yes  </td><td>&nbsp;</td><td>80(1+63+16)</td><td>80</td></tr>
<tr><td>float128  </td><td>Floating point - fixed size </td><td>Yes  </td><td>&nbsp;</td><td>128(1+112+15)</td><td>128</td></tr>

<tr><td>cfloat    </td><td>Floating point - system size</td><td>Yes  </td><td>&nbsp;</td><td>32(1+23+8)</td><td>&infin;</td></tr>
<tr><td>cdouble   </td><td>Floating point - system size</td><td>Yes  </td><td>&nbsp;</td><td>64(1+52+11)</td><td>&infin;</td></tr>
<tr><td>clongdouble</td><td>Floating point - system size</td><td>Yes  </td><td>&nbsp;</td><td>64(1+52+11)</td><td>&infin;</td></tr>

<tr><td>bool      </td><td>Boolean              </td><td>&nbsp;</td><td>&nbsp;</td><td>1+15</td><td>&infin;</td></tr>
</table>


<!--
        { 11, 11, 16, 16,  1,  0,  0,  0 }, /* float16 - IEEE 754-2008 */
        { 24, 24, 32, 32,  1,  0,  0,  0 }, /* float32 - IEEE 754 */
        { 53, 53, 64, 64,  1,  0,  0,  0 }, /* float64 - IEEE 754 */
        { 64, 64, 80, 80,  1,  0,  0,  0 }, /* float80 - x86 extended */
        {113,113,128,128,  1,  0,  0,  0 }, /* float128 - IEEE 754-2008  */
-->

**TODO**

Array types
-----------
Syntax:

    elemtype#[size]
    elemtype#[size,size...]

**TODO**
**NOTE** the qualifier is placed before the element type, not before the #[

Bitfield types
--------------
Syntax:

    basetype bits (bitfields...)

Bitfield syntax:

    ident
    N bits ident
    N bits typeoverride ident

**TODO**
**NOTE** the base type is optional
**NOTE** trailing comma allowed
**NOTE** should be used carfully with shared/volatile

Enumeration types
-----------------
Syntax:

    basetype enum(name1=value1, name2=value2...)

**TODO**
**NOTE** the base type is optional
**NOTE** the values are optional
**NOTE** trailing comma allowed

Function types
--------------
Syntax:

    flags returntype()
    flags returntype(arguments....)
    
**TODO**
**NOTE** flags = noreturn, optional
**NOTE** arguments may contain names also

Optional types
--------------
Syntax:

    type?

**TODO**
**NOTE** optional pointer types are stored without any extra overhead. other types, including "optional optional pointer types" require wrapping in a struct with a boolean flag before the actual value.

Pointer types
-------------
Syntax:

    type^
    type+>
    type*>
    type+*>

**TODO**

Struct types
------------
Syntax:

    ()
    (type1 name1)
    (type1 name1, type2 name2...)

**TODO**
**NOTE** names are optional
**NOTE** trailing comma is allowed

Union types
-----------
Syntax:

    ()
    (type1 name1)
    (type1 name1, type2 name2...)

**TODO**
**NOTE** names are optional
**NOTE** trailing comma is allowed
**TODO** it should be possible to have "locked down" unions where only one member can ever be accessed through its life-time (disallowing so called "type punning"). perhaps it should be the default (the alternative could be called e.g. "multiunion" or "punningunion")

Parametric types
----------------
Syntax:

    identifiertype[T]
    identifiertype[T,U...]

**TODO**
**NOTE** type param don't have to be T and U. can be any identifier (and more than 1 character)

any types
---------
Syntax:

    any

**TODO**
**NOTE** can be any type EXCEPT function types! (perhaps we should have a "anyfunction" type?)
**NOTE** can only be used in pointers

private types
-------------
Syntax:

    private

**TODO**

