
add two new qualifiers:
    - writeonly
    - noaccess

a noaccess pointer can still be used in comparisons and can be passed around, but not dereferenced.
