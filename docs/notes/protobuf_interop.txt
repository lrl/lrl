LRL could have an interop for Protobuf. Version 3 of protobuf seems to be a
simplification of version 2, so it's probably better for focus on version 3.
(On the other hand, their project page says version 2 support will stay for
a long time, so it might be good to have support for version 2 as well).
