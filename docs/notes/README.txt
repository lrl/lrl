Some of these files are not up to date! They represent my thoughts at 
some time in the past. Things may have changed since then :)

Have a look at the test suite (compiler/tests/parser/*.good) for up to 
date examples of how LRL code can look like.
